<%-- 
    Document   : login
    Created on : 04/10/2015, 08:22:58 AM
    Author     : Mario
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
  <head>
        <link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css'/>">
        <link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-theme.min.css'/>">
        <link rel="stylesheet" href="<c:url value='/resources/css/simple.css'/>"/>
        <link rel="stylesheet" href="<c:url value='/resources/css/project_style.css'  />"/>
  </head>        
  <body>
        <div>
            <form name ="form" id="forma-login" class="marco" method="POST" action="${contextPath}/login" >
                <!--label for="username" class="uname" data-icon="u" > Your email or username </label-->
                <div class="row">
                    <div class="col-md-offset-4 col-md-2"><img  height="50px" width="100px" src="<c:url value='/resources/img/GNKL_Small.JPG' />" ></div>
                    <div class="col-md-4"><h2><spring:message code='header.message'/></h2></div>
                </div>
                <div class="row">
                    <div class="col-sm-offset-4 col-sm-4">
                        <h4>Validar Usuario</h4>
                    </div>
                </div>

                <div class="form-group col-sm-offset-4 col-sm-4 ${error != null ? 'has-error' : ''}">
                    <c:if test="${message!=null}">
                        <div class="alert alert-info">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>${message}</span>
                        </div>
                    </c:if>
                    <input name="username" type="text" class="form-control" placeholder="Username"
                           autofocus="true"/>
                    <br>
                    <input name="password" type="password" class="form-control" placeholder="Password"/>
                    <br>
                    <c:if test="${error!=null}">
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>${error}</span>
                        </div>
                    </c:if>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
                </div>                    
            </form>
        </div>
  </body>
</html>  
