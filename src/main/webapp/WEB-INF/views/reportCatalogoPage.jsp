<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
  <head>  
    <title>Reporteador</title>  
        <link rel="stylesheet" href="<c:url value='/resources/css/jquery.dataTables.min.css'/>">
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.css">
        <link rel="stylesheet" href="<c:url value='/resources/css/select.min.css'/>">
        <link rel="stylesheet" href="<c:url value='/resources/css/sweetalert.css'/>"/>
        <link rel="stylesheet" href="<c:url value='/resources/css/simple.css'/>"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">    
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">        
        <!--<link rel="stylesheet" href="<c:url value='/resources/css/selectize.default.css'/>">-->
        <link rel="stylesheet" href="<c:url value='/resources/css/project_style.css'  />"/>    
        
        
    
        <script src="<c:url value='/resources/js/jquery-2.1.4.min.js'/>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.js"></script>
        <script src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>
        
        <script src="<c:url value='/resources/js/angular-route.js'/>"></script>
        <script src="<c:url value='/resources/js/angular-touch.js'/>"></script>
        <script src="<c:url value='/resources/js/angular-animate.js'/>"></script>

        <script src="<c:url value='/resources/js/smart-table.js'/>"></script>
        <script src="<c:url value='/resources/js/ui-bootstrap-tpls.min.js'/>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-sanitize.js"></script>
        <script src="<c:url value='/resources/js/select.js'  />"></script>
        <script src="<c:url value='/resources/js/sweetalert.min.js'  />"></script>
        <script src="<c:url value='/resources/js/ngSweetAlert.js'  />"></script>
        <script src="<c:url value='/resources/js/angular-locale_es-mx.js'/>"></script>
        
        <script src="<c:url value='/resources/js/reporteador/reporteador.js' />"></script>
        <script src="<c:url value='/resources/js/reporteador/service/catalogo_service.js' />"></script>
        <script src="<c:url value='/resources/js/reporteador/controller/catalogo_controller.js' />"></script>
        
  </head>
  
  <body ng-app="reporteador" class="ng-cloak">
    <div id="loadingModal" class="modal fade in centering" aria-labelledby="" aria-hidden="true" tabindex="-1">
        <img src="<c:url value='/resources/img/ajax-loader.gif' />"/>
    </div>      
      <div class="generic-container" ng-controller="CatalogoController as ctrl">          
          <div class="panel panel-default">
              <div class="panel-heading">
                  <div class="form-group row left">
                    <span class="lead col-sm-2">Reporteador</span>
                    <!--<img class="" src="<c:url value='/resources/img/LogoGNKclaro2.jpg' />" width="200" height="100"/>-->                      
                  </div>
                    <strong>${user}</strong>, Bienvenido.&nbsp;
                    <a href="<c:url value="/logout" />">Logout</a>&nbsp;
                    <a href="<c:url value="/welcome" />">Regresar</a>
              </div>
              
              <div class="container">
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label">CEDIS:</label>
                    <div  class="col-sm-10">
                        <ui-select ng-model="selectedCedis"  theme="bootstrap" name="cedis" on-select="onSelectCedisCallback($item, $model);">
                            <ui-select-match placeholder="Seleccionar CEDIS" class="ui-select-match">
                                <span ng-bind="$select.selected.desc"></span>
                            </ui-select-match>
                            <ui-select-choices class="ui-select-choices" repeat="obj in cedisParameters">
                                <small ng-bind-html="obj.desc | highlight: $select.search"></small>
                            </ui-select-choices>
                        </ui-select>                        
                    </div>
                </div>                  
              </div>
              
              <div id="exTab2" class="container" ng-show="ctrl.enableFormUnidad">
                <br>    
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a  href="#1" data-toggle="tab">Medicamento</a>
                            </li>
                            <li>
                                <a href="#2" data-toggle="tab">Unidades</a>
                            </li>
                        </ul>
                        <div class="tab-content ">
                            <div class="tab-pane active" id="1">
                                <br>    
                                <div class="formcontainer">
                                    <form name="catalogoForm" class="form-horizontal">
                                      <fieldset>                                           
                                          <div class="form-group row">
                                              <label class="col-sm-2 form-control-label">Tipo medicamento</label>
                                              <div  class="col-sm-6">
                                                  <div class="input-group">
                                                      <ui-select ng-model="ctrl.tipoMedicamento"  theme="bootstrap" name="idtipomedicamento" on-select="onSelectTipoMedicamentoCallback($item, $model);" ng-disabled="alltipos">
                                                        <ui-select-match placeholder="Seleccionar tipo medicamento" class="ui-select-match">
                                                             <span ng-bind="$select.selected.clave+' '+$select.selected.nombre"></span>
                                                        </ui-select-match>
                                                        <ui-select-choices class="ui-select-choices" repeat="obj in listTipo">
                                                          <small ng-bind-html="obj.clave+' '+obj.nombre | highlight: $select.search"></small>
                                                        </ui-select-choices>
                                                      </ui-select>
                                                      <span class="input-group-btn">
                                                        <button type="button" ng-click="ctrl.tipoMedicamento = undefined" class="btn btn-default">
                                                          <span class="glyphicon glyphicon-trash"></span>
                                                        </button>
                                                      </span>                                    
                                                  </div>                    
                                              </div>
                                              <div  class="col-sm-3">
                                                  <label>
                                                    <input type="checkbox" ng-model="alltipos" ng-change="eventChangeAllTipoMedicamentoClick(alltipos)">Todos los tipos&nbsp;
                                                  </label>  
                                              </div>
                                          </div>
                                          <div class="form-group row">
                                              <label class="col-sm-2 form-control-label">Parámetros</label>
                                              <div  class="col-sm-10">
                                                  <ui-select multiple theme="bootstrap" ng-model="ctrl.selectedMedicamentoColumns" name="parametros" theme="bootstrap" close-on-select="false" title="Escoger parametros" remove-selected="false">
                                                    <ui-select-match placeholder="Seleccionar parametros" class="ui-select-match">
                                                        <span ng-bind="$item.desc"></span>
                                                    </ui-select-match>
                                                    <ui-select-choices repeat="column in columnMedicamentoParameters | filter: $select.search" class="ui-select-choices">
                                                      <div ng-bind-html="column.desc | highlight: $select.search"></div>
                                                    </ui-select-choices>
                                                  </ui-select>
                                              </div>
                                          </div>                        
                                      </fieldset>
                                      <div class="modal-footer">
                                          <input class="btn btn-primary" ng-click="searchMedicamentoDataByParams(catalogoForm)" value="Buscar" type="submit">
                                          <button class="btn btn-default" data-dismiss="modal" ng-click="clearDataByParams()" aria-hidden="true">Limpiar</button>
                                          <button class="btn btn-info" data-dismiss="modal" ng-click="createReporteMedicamento('Reporte')" aria-hidden="true">Exportar</button>
                                      </div>
                                    </form>

                                      <div class="form-horizontal">
                                          <label>Total registros: {{totalQueryResult | number}}</label>
                                          <br>
                                          <div class="form-group">
                                              <label ng-if="summaryObj['F_Costo']!=null && summaryObj['F_Costo']!=undefined" class="col-sm-3">$ Costo:&nbsp;{{summaryObj['F_Costo'] | number}}&nbsp;</label>
                                          </div>
                                      </div>

                                      <table id="tableMedicamento" st-table="tableMedicamentoData" class="table table-striped" st-pipe="serverMedicamentoBusquedaFilter">
                                          <thead>
                                              <tr>
                                                  <th st-sort={{name.column}} ng-repeat="name in tableMedicaHeader" class="firstRow">{{name.desc}}</th>
                                                  <th class="firstRow"></th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              <tr ng-repeat="row in tableMedicamentoData ">
                                                  <td ng-repeat="column in row">{{column}}</td>
                                              </tr>
                                          </tbody>
                                          <tfoot>
                                              <tr>
                                                  <td colspan="{{tableMedicaHeader.length}}">
                                                      <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                                                  </td>
                                              </tr>
                                          </tfoot>
                                      </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="2">
                                <br>
                                <div class="formcontainer">
                                    <form name="unidadesForm" class="form-horizontal">
                                      <fieldset>
                                          <div class="form-group row">
                                              <label class="col-sm-2 form-control-label">Tipo unidad</label>
                                              <div  class="col-sm-6">
                                                  <div class="input-group">
                                                      <ui-select ng-model="ctrl.tipoUnidad"  theme="bootstrap" name="idtipounidad" on-select="onSelectTipoUnidadCallback($item, $model);" ng-disabled="alltipounidad">
                                                        <ui-select-match placeholder="Seleccionar tipo unidad" class="ui-select-match">
                                                             <span ng-bind="$select.selected.clave+' '+$select.selected.nombre"></span>
                                                        </ui-select-match>
                                                        <ui-select-choices class="ui-select-choices" repeat="obj in listTipoUnidad">
                                                          <small ng-bind-html="obj.clave+' '+obj.nombre | highlight: $select.search"></small>
                                                        </ui-select-choices>
                                                      </ui-select>
                                                      <span class="input-group-btn">
                                                        <button type="button" ng-click="ctrl.tipoUnidad = undefined" class="btn btn-default">
                                                          <span class="glyphicon glyphicon-trash"></span>
                                                        </button>
                                                      </span>                                    
                                                  </div>                    
                                              </div>
                                              <div  class="col-sm-3">
                                                  <label>
                                                    <input type="checkbox" ng-model="alltipounidad" ng-change="eventChangeAllTipoUnidadClick(alltipounidad)">Todos los tipos&nbsp;
                                                  </label>  
                                              </div>
                                          </div>
                                          <div class="form-group row">
                                              <label class="col-sm-2 form-control-label">Parámetros</label>
                                              <div  class="col-sm-10">
                                                  <ui-select multiple theme="bootstrap" ng-model="ctrl.selectedUnidadColumns" name="parametros" theme="bootstrap" close-on-select="false" title="Escoger parametros" remove-selected="false">
                                                    <ui-select-match placeholder="Seleccionar parametros" class="ui-select-match">
                                                        <span ng-bind="$item.desc"></span>
                                                    </ui-select-match>
                                                    <ui-select-choices repeat="column in columnUnidadParameters | filter: $select.search" class="ui-select-choices">
                                                      <div ng-bind-html="column.desc | highlight: $select.search"></div>
                                                    </ui-select-choices>
                                                  </ui-select>
                                              </div>
                                          </div>                                          
                                      </fieldset>
                                      <div class="modal-footer">
                                          <input class="btn btn-primary" ng-click="searchUnidadDataByParams(unidadesForm)" value="Buscar" type="submit">
                                          <button class="btn btn-default" data-dismiss="modal" ng-click="clearDataUnidad()" aria-hidden="true">Limpiar</button>
                                          <button class="btn btn-info" data-dismiss="modal" ng-click="createReporteUnidad('Reporte')" aria-hidden="true">Exportar</button>
                                      </div>
                                    </form>

                                      <div class="form-horizontal">
                                          <label>Total registros: {{ctrl.totalUnidadQuery | number}}</label>
                                          <br>
                                      </div>
                                    
                                      <table id="tableUnidad" st-table="tableUnidadData" class="table table-striped" st-pipe="serverUnidadBusquedaFilter">
                                          <thead>
                                              <tr>
                                                  <th st-sort={{name.column}} ng-repeat="name in tableUnidadHeader" class="firstRow">{{name.desc}}</th>
                                                  <th class="firstRow"></th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              <tr ng-repeat="row in tableUnidadData ">
                                                  <td ng-repeat="column in row">{{column}}</td>
                                              </tr>
                                          </tbody>
                                          <tfoot>
                                              <tr>
                                                  <td colspan="{{tableUnidadHeader.length}}">
                                                      <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                                                  </td>
                                              </tr>
                                          </tfoot>
                                      </table>                                    

                                </div>
                            </div>                            
                        </div>
                  </div>            
          </div>
      </div>
  </body>
</html>