<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
  <head>  
    <title>Reporteador</title>  
        <link rel="stylesheet" href="<c:url value='/resources/css/jquery.dataTables.min.css'/>">
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.css">
        <link rel="stylesheet" href="<c:url value='/resources/css/select.min.css'/>">
        <link rel="stylesheet" href="<c:url value='/resources/css/sweetalert.css'/>"/>
        <link rel="stylesheet" href="<c:url value='/resources/css/simple.css'/>"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">    
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">        
        <!--<link rel="stylesheet" href="<c:url value='/resources/css/selectize.default.css'/>">-->
        <link rel="stylesheet" href="<c:url value='/resources/css/project_style.css'  />"/>    
        
        
    
        <script src="<c:url value='/resources/js/jquery-2.1.4.min.js'/>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.js"></script>
        <script src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>
        
        <script src="<c:url value='/resources/js/angular-route.js'/>"></script>
        <script src="<c:url value='/resources/js/angular-touch.js'/>"></script>
        <script src="<c:url value='/resources/js/angular-animate.js'/>"></script>

        <script src="<c:url value='/resources/js/smart-table.js'/>"></script>
        <script src="<c:url value='/resources/js/ui-bootstrap-tpls.min.js'/>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-sanitize.js"></script>
        <script src="<c:url value='/resources/js/select.js'  />"></script>
        <script src="<c:url value='/resources/js/sweetalert.min.js'  />"></script>
        <script src="<c:url value='/resources/js/ngSweetAlert.js'  />"></script>
        <script src="<c:url value='/resources/js/angular-locale_es-mx.js'/>"></script>
        
        <script src="<c:url value='/resources/js/reporteador/reporteador.js' />"></script>
        <script src="<c:url value='/resources/js/reporteador/service/recibo_service.js' />"></script>
        <script src="<c:url value='/resources/js/reporteador/controller/recibo_controller.js' />"></script>
        
  </head>
  
  <body ng-app="reporteador" class="ng-cloak">
    <div id="loadingModal" class="modal fade in centering" aria-labelledby="" aria-hidden="true" tabindex="-1">
        <img src="<c:url value='/resources/img/ajax-loader.gif' />"/>
    </div>      
      <div class="generic-container" ng-controller="ReciboController as ctrl">
          <div class="panel panel-default">
              <div class="panel-heading">
                  <div class="form-group row left">
                    <span class="lead col-sm-2">Reporteador</span>
                    <!--<img class="" src="<c:url value='/resources/img/LogoGNKclaro2.jpg' />" width="200" height="100"/>-->                      
                  </div>
                    <strong>${user}</strong>, Bienvenido.&nbsp;
                    <a href="<c:url value="/logout" />">Logout</a>&nbsp;
                    <a href="<c:url value="/welcome" />">Regresar</a>
              </div>
              
              <div class="container">
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label">CEDIS:</label>
                    <div  class="col-sm-10">
                        <ui-select ng-model="ctrl.selectedCedis"  theme="bootstrap" name="cedis" on-select="onSelectCedisCallback($item, $model);">
                            <ui-select-match placeholder="Seleccionar CEDIS" class="ui-select-match">
                                <span ng-bind="$select.selected.desc"></span>
                            </ui-select-match>
                            <ui-select-choices class="ui-select-choices" repeat="obj in cedisParameters">
                                <small ng-bind-html="obj.desc | highlight: $select.search"></small>
                            </ui-select-choices>
                        </ui-select>                        
                    </div>
                </div>                  
              </div>              
                            
              <br>
              
              <div class="formcontainer" ng-show="ctrl.enableFormRecibo">
                  <form name="reciboForm" class="form-horizontal">
                    <fieldset>        
                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Proveedor</label>
                            <div  class="col-sm-6">
                                <div class="input-group">
                                    <ui-select ng-model="ctrl.proveedor"  theme="bootstrap" name="idproveedor" on-select="onSelectProveedorCallback($item, $model);" ng-disabled="allUnidades">
                                      <ui-select-match placeholder="Seleccionar proveedor" class="ui-select-match">
                                           <span ng-bind="$select.selected.clave+' '+$select.selected.nombre"></span>
                                      </ui-select-match>
                                      <ui-select-choices class="ui-select-choices" refresh="getListProveedor($select)" repeat="obj in listProveedor">
                                        <small ng-bind-html="obj.clave+' '+obj.nombre | highlight: $select.search"></small>
                                      </ui-select-choices>
                                    </ui-select>
                                    <span class="input-group-btn">
                                      <button type="button" ng-click="ctrl.proveedor = undefined" class="btn btn-default">
                                        <span class="glyphicon glyphicon-trash"></span>
                                      </button>
                                    </span>                                    
                                </div>                    
                            </div>
                            <div  class="col-sm-3">
                                <label>
                                  <input type="checkbox" ng-model="allUnidades" ng-change="eventChangeAllUnidadesClick(allUnidades)">Todos los proveedores&nbsp;
                                </label>  
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Clave: </label>
                            <div  class="col-sm-10">
                                <div class="input-group">
                                    <ui-select ng-model="ctrl.clave" theme="bootstrap" title="Claves" name="idclaveproveedor" on-select="onSelectClaveCallback($item, $model);">
                                        <ui-select-match placeholder="Seleccionar clave" class="ui-select-match">
                                            <span ng-bind="$select.selected.clave+' '+$select.selected.descripcion"></span>
                                        </ui-select-match>
                                        <ui-select-choices repeat="obj in listClaves | filter: $select.search" class="ui-select-choices">
                                          <span ng-bind-html="obj.clave+' '+obj.descripcion | highlight: $select.search"></span>
                                        </ui-select-choices>
                                     </ui-select> 
                                    <span class="input-group-btn">
                                      <button type="button" ng-click="ctrl.clave = undefined" class="btn btn-default">
                                        <span class="glyphicon glyphicon-trash"></span>
                                      </button>
                                    </span>                                       
                                </div>                    
                            </div>                    
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Parámetros</label>
                            <div  class="col-sm-10">
                                <ui-select multiple theme="bootstrap" ng-model="ctrl.selectedColumns" name="parametros" theme="bootstrap" close-on-select="false" title="Escoger parametros" remove-selected="false">
                                  <ui-select-match placeholder="Seleccionar parametros" class="ui-select-match">
                                      <span ng-bind="$item.desc"></span>
                                  </ui-select-match>
                                  <ui-select-choices repeat="column in columnParameters | filter: $select.search" class="ui-select-choices">
                                    <div ng-bind-html="column.desc | highlight: $select.search"></div>
                                  </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 form-control-label">Fechas</label>
                            <div class="col-sm-4">                  
                                <p class="input-group ">
                                    <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="fechaInicio" is-open="popup1.opened" datepicker-options="dateOptions" name="fechaInicio" close-text="Close" ng-readonly="true" ng-required="true"/>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
                                    </span>
                                </p>                       
                            </div>                                
                            <div class="col-sm-4">
                                    <p class="input-group">
                                        <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="fechaFin" is-open="popup2.opened" datepicker-options="{minDate: fechaInicio}" close-text="Close" alt-input-formats="altInputFormats" name="fechaFin" ng-readonly="true" ng-required="true"/>
                                        <span class="input-group-btn">
                                          <button type="button" class="btn btn-default" ng-click="open2()"><i class="glyphicon glyphicon-calendar"></i></button>
                                        </span>
                                    </p>                    
                            </div>                                              
                        </div>
                    </fieldset>
                    <div class="modal-footer">
                        <input class="btn btn-primary" ng-click="searchDataByParams(reciboForm)" value="Buscar" type="submit">
                        <button class="btn btn-default" data-dismiss="modal" ng-click="clearDataByParams()" aria-hidden="true">Limpiar</button>
                        <button class="btn btn-info" data-dismiss="modal" ng-click="createReporteBitacoraStatus('Reporte')" aria-hidden="true">Exportar</button>
                    </div>
                  </form>
                  
                    <div class="form-horizontal">
                        <label>Total registros: {{totalQueryResult | number}}</label>
                        <br>
                        <div class="form-group">
                            <label ng-if="summaryObj['F_Costo']!=null && summaryObj['F_Costo']!=undefined" class="col-sm-3">$ Costo:&nbsp;{{summaryObj['F_Costo'] | number}}&nbsp;</label>
                            <label ng-if="summaryObj['F_ClaDoc']!=null && summaryObj['F_ClaDoc']!=undefined" class="col-sm-2">Folios:&nbsp;{{summaryObj['F_ClaDoc'] | number}}&nbsp;</label>                            
                            <label ng-if="summaryObj['F_CanCom']!=null && summaryObj['F_CanCom']!=undefined" class="col-sm-3">Cantidad compra:&nbsp;{{summaryObj['F_CanCom'] | number}}&nbsp;</label>
                            <!--<label ng-if="summaryObj['F_ComTot']!=null && summaryObj['F_ComTot']!=undefined" class="col-sm-3">$ Compra total:&nbsp;{{summaryObj['F_ComTot'] | number}}&nbsp;</label>-->
                            <label ng-if="summaryObj['F_OrdCom']!=null && summaryObj['F_OrdCom']!=undefined" class="col-sm-3">Total de ordenes de compra:&nbsp;{{summaryObj['F_OrdCom'] | number}}&nbsp;</label>
                            <label ng-if="summaryObj['F_Cajas']!=null && summaryObj['F_Cajas']!=undefined" class="col-sm-2">Cajas:&nbsp;{{summaryObj['F_Cajas'] | number}}&nbsp;</label>
                            <label ng-if="summaryObj['F_ImpTo']!=null && summaryObj['F_ImpTo']!=undefined" class="col-sm-3">$ Importe total:&nbsp;{{summaryObj['F_ImpTo'] | number}}&nbsp;</label>
                        </div>
                    </div>

                    <table st-table="tableData" class="table table-striped" st-pipe="serverBusquedaFilter">
                        <thead>
                            <tr>
                                <th st-sort={{name.column}} ng-repeat="name in tableHeader" class="firstRow">{{name.desc}}</th>
                                <th class="firstRow"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="row in tableData ">
                                <td ng-repeat="column in row">{{column}}</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="{{tableHeader.length}}">
                                    <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
              </div>
          </div>
      </div>
  </body>
</html>