<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
  <head>  
    <title>Reporteador</title>  
        <link rel="stylesheet" href="<c:url value='/resources/css/jquery.dataTables.min.css'/>">
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.css">
        <link rel="stylesheet" href="<c:url value='/resources/css/select.min.css'/>">
        <link rel="stylesheet" href="<c:url value='/resources/css/sweetalert.css'/>"/>
        <link rel="stylesheet" href="<c:url value='/resources/css/simple.css'/>"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">    
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">        
        <!--<link rel="stylesheet" href="<c:url value='/resources/css/selectize.default.css'/>">-->
        <link rel="stylesheet" href="<c:url value='/resources/css/project_style.css'  />"/>    
        
        
    
        <script src="<c:url value='/resources/js/jquery-2.1.4.min.js'/>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.js"></script>
        <script src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-sanitize.js"></script>

        
        
  </head>
  
  <body>
      <div class="generic-container">
              <div class="panel-heading">
                  <div class="form-group row left">
                    <span class="lead col-sm-2">Reporteador</span>
                    <img class="" src="<c:url value='/resources/img/LogoGNKclaro2.jpg' />" width="200" height="100"/>
                  </div>
                  <div class="form-group row left">
                    <a href="<c:url value="/reporter" />">Reporte Entregas</a><br>
                    <a href="<c:url value="/recibo" />">Reporte Recibo</a><br>
                    <a href="<c:url value="/catalogo" />">Reporte Catálogos</a><br>
                  </div>
              </div>
      </div>
  </body>
</html>