/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
 
angular.module('reporteador').factory('ReporterService', ['$http', '$q', function($http, $q){
        //var REST_SERVICE_URI = 'http://52.43.26.76:8080/GnklReporteador/reporter/';
        var REST_SERVICE_URI = '../GnklReporteador/reporter/';
        var REST_INDEX_SERVICE_URI = '../GnklReporteador/index/cedis';
        
        var factory = {
            getAllUnidadMedica: getAllUnidadMedica,
            getAllClaveByUnidadMedica: getAllClavesByUnidadMedica,
            getAllClaves: getAllClaves,
            getAllUnidadMedicaByName: getAllUnidadMedicaByName,
            getSearchDataByParams: getSearchDataByParams,
            getSummaryByParams:getSummaryByParams,
            getReporteByParams:getReporteByParams,
            setCedisData:setCedisData
        };

        return factory;
        
        function getSearchDataByParams(idUnidad, idClave, parameterList, fechaInicio, fechaFin, page, offset) {          
            var url = REST_SERVICE_URI+'searchByParams';
            var deferred = $q.defer();
            var parameters = [];
            for(var i=0;i<parameterList.length;i++){
               var obj= parameterList[i];
               parameters.push(obj.column);
            }
            console.log(parameters);
            $http({
                method: 'GET',
                url: url,
                params:{
                    'idUnidad':idUnidad, 
                    'idClave':idClave, 
                    'parametros':parameters,
                    'fechaInicio':fechaInicio,
                    'fechaFin':fechaFin,
                    'page':page,
                    'offset':offset
                }
            })
                .then(
                function (response) {
                    deferred.resolve(response);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }        
        
        function getSummaryByParams(idUnidad, idClave, parameterList, fechaInicio, fechaFin) {          
            var url = REST_SERVICE_URI+'getSummatoryByParams';
            var deferred = $q.defer();
            var parameters = [];
            for(var i=0;i<parameterList.length;i++){
               var obj= parameterList[i];
               parameters.push(obj.column);
            }
            console.log(parameters);
            $http({
                method: 'GET',
                url: url,
                params:{
                    'idUnidad':idUnidad, 
                    'idClave':idClave, 
                    'parametros':parameters,
                    'fechaInicio':fechaInicio,
                    'fechaFin':fechaFin
                }
            })
                .then(
                function (response) {
                    deferred.resolve(response);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }        
        
        function getAllUnidadMedica() {
            var url = REST_SERVICE_URI+'unidades';
            var deferred = $q.defer();
            $http({method: 'GET',url: url})
                .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }

        function getAllUnidadMedicaByName(name) {
            var url = REST_SERVICE_URI+'unidadesbyname';
            var deferred = $q.defer();
            $http({method: 'GET',
                url: url, 
                params:{'name':name}})
                .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }        
        
        function getAllClavesByUnidadMedica(idUnidad){
            var url = REST_SERVICE_URI+'clavesbyunidad';
            var deferred = $q.defer();
            $http({method: 'GET',
                url: url, 
                params:{'unidad':idUnidad}})
                .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }
        
        function getAllClaves(){
            var url = REST_SERVICE_URI+'allclaves';
            var deferred = $q.defer();
            $http({method: 'GET',
                url: url
                }).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }
        
        function getReporteByParams(idUnidad, idClave, parameterList, fechaInicio, fechaFin) {          
            var url = REST_SERVICE_URI+'getReporteByParameters';
            var deferred = $q.defer();
            var parameters = [];
            for(var i=0;i<parameterList.length;i++){
               var obj= parameterList[i];
               parameters.push(obj.column);
            }
            //$http.get(url, {responseType: 'arraybuffer',params:{'tipoEnvio':$scope.tipoEnvio,'listStatus':$scope.estatusRow, 'filename':fileName}})
            console.log(parameters);
            $http({
                method: 'GET',
                url: url,
                responseType: 'arraybuffer',
                params:{
                    'idUnidad':idUnidad, 
                    'idClave':idClave, 
                    'parametros':parameters,
                    'fechaInicio':fechaInicio,
                    'fechaFin':fechaFin
                }
            })
                .then(
                function (response) {
                    deferred.resolve(response);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }
        
        function setCedisData(cedis){
            var url = REST_INDEX_SERVICE_URI;
            var deferred = $q.defer();
            $http({method: 'GET',
                url: url,
                params:{
                    'cedis':cedis,
                }
                }).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;            
        }        
        
}]);

