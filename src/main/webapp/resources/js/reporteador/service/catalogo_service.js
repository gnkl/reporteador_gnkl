/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
 
angular.module('reporteador').factory('CatalogoService', ['$http', '$q', function($http, $q){
        //var REST_RECIBO_SERVICE_URI = 'http://52.43.26.76:8080/GnklReporteador/medicamento/';
        //var REST_UNIDAD_SERVICE_URI = 'http://52.43.26.76:8080/GnklReporteador/unidad/';
        var REST_RECIBO_SERVICE_URI = '../GnklReporteador/medicamento/';
        var REST_UNIDAD_SERVICE_URI = '../GnklReporteador/unidad/';
        var REST_INDEX_SERVICE_URI = '../GnklReporteador/index/cedis';
        
        var factory = {
            getAllTipoMedicamento:getAllTipoMedicamento,
            getSearchDataByParams:getSearchDataByParams,
            getSummaryByParams:getSummaryByParams,
            getAllTipoUnidad:getAllTipoUnidad,
            getSearchUnidadDataByParams:getSearchUnidadDataByParams,
            getSummaryUnidadByParams:getSummaryUnidadByParams,
            getListDispensadores:getListDispensadores,
            setCedisData:setCedisData,
            getReporteMedicaByParams:getReporteMedicaByParams,
            getReporteUnidadByParams:getReporteUnidadByParams
        };

        return factory;
        
        function getSearchDataByParams(tipo, parameterList, page, offset) {          
            var url = REST_RECIBO_SERVICE_URI+'listmedicamento';
            var deferred = $q.defer();
            var parameters = [];

            for(var i=0;i<parameterList.length;i++){
               var obj= parameterList[i];
               parameters.push(obj.column);
            }

            $http({
                method: 'GET',
                url: url,
                params:{
                    'tipo':(tipo!=null && tipo!=undefined)?tipo.clave:null, 
                    'parametros':parameters,
                    'page':page,
                    'offset':offset
                }
            })
                .then(
                function (response) {
                    deferred.resolve(response);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }
        
        function getSearchUnidadDataByParams(tipo, municipio, parameterList, page, offset){
            var url = REST_UNIDAD_SERVICE_URI+'listunidad';
            var deferred = $q.defer();
            var parameters = [];

            for(var i=0;i<parameterList.length;i++){
               var obj= parameterList[i];
               parameters.push(obj.column);
            }

            console.log(tipo);
            
            $http({
                method: 'GET',
                url: url,
                params:{
                    'tipo':tipo, 
                    'municipio':municipio,
                    'parametros':parameters,
                    'page':page,
                    'offset':offset,
                    'dispensador':''
                }
            })
                .then(
                function (response) {
                    deferred.resolve(response);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;            
        }
        
        function getSummaryByParams(tipo, parameterList) {          
            var url = REST_RECIBO_SERVICE_URI+'summatorymedicamento';
            var deferred = $q.defer();
            var parameters = [];
            for(var i=0;i<parameterList.length;i++){
               var obj= parameterList[i];
               parameters.push(obj.column);
            }

            $http({
                method: 'GET',
                url: url,
                params:{
                    'tipo':(tipo!=null && tipo!=undefined)?tipo.clave:null, 
                    'parametros':parameters,
                }
            })
                .then(
                function (response) {
                    deferred.resolve(response);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }        
        
        function getSummaryUnidadByParams(tipo, municipio, parameterList) {          
            var url = REST_UNIDAD_SERVICE_URI+'summatoryunidad';
            var deferred = $q.defer();
            var parameters = [];
            for(var i=0;i<parameterList.length;i++){
               var obj= parameterList[i];
               parameters.push(obj.column);
            }
            console.log(parameters);
            $http({
                method: 'GET',
                url: url,
                params:{
                    'tipo':tipo,
                    'municipio':municipio,
                    'parametros':parameters,
                    'dispensador':''
                }
            })
                .then(
                function (response) {
                    deferred.resolve(response);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }
                
        function getAllTipoMedicamento(){
            var url = REST_RECIBO_SERVICE_URI+'tipomedicamento';
            var deferred = $q.defer();
            $http({method: 'GET',
                url: url
                }).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }        

        function getAllTipoUnidad(){
            var url = REST_UNIDAD_SERVICE_URI+'tipounidad';
            var deferred = $q.defer();
            $http({method: 'GET',
                url: url
                }).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }
        
        function getListDispensadores(){
            var url = REST_UNIDAD_SERVICE_URI+'getalldispensadores';
            var deferred = $q.defer();
            $http({method: 'GET',
                url: url
                }).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;            
        }
        
        function setCedisData(cedis){
            var url = REST_INDEX_SERVICE_URI;
            var deferred = $q.defer();
            $http({method: 'GET',
                url: url,
                params:{
                    'cedis':cedis,
                }
                }).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;            
        }
        
        function getReporteMedicaByParams(tipo, tableMedicaHeader){
            var url = REST_RECIBO_SERVICE_URI+'getReporteByParameters';
            var deferred = $q.defer();
            var parameters = [];
            for(var i=0;i<tableMedicaHeader.length;i++){
               var obj= tableMedicaHeader[i];
               parameters.push(obj.column);
            }
            console.log(parameters);
            $http({
                method: 'GET',
                url: url,
                responseType: 'arraybuffer',
                params:{
                    'tipo':(tipo!=null && tipo!=undefined)?tipo.clave:null, 
                    'parametros':parameters
                }
            }).then(
                function (response) {
                    deferred.resolve(response);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;            
        }
        
        function getReporteUnidadByParams(tipo, tableUnidadHeader){
            var url = REST_UNIDAD_SERVICE_URI+'getReporteByParameters';
            var deferred = $q.defer();
            var parameters = [];
            for(var i=0;i<tableUnidadHeader.length;i++){
               var obj= tableUnidadHeader[i];
               parameters.push(obj.column);
            }
            //$http.get(url, {responseType: 'arraybuffer',params:{'tipoEnvio':$scope.tipoEnvio,'listStatus':$scope.estatusRow, 'filename':fileName}})
            console.log(parameters);
            $http({
                method: 'GET',
                url: url,
                responseType: 'arraybuffer',
                params:{
                    'tipo':tipo, 
                    'parametros':parameters
                }
            }).then(
                function (response) {
                    deferred.resolve(response);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;            
        }
}]);

