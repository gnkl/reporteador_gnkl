/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
 
angular.module('reporteador').factory('ReciboService', ['$http', '$q', function($http, $q){
        //var REST_RECIBO_SERVICE_URI = 'http://52.43.26.76:8080/GnklReporteador/recibo/';
        var REST_RECIBO_SERVICE_URI = '../GnklReporteador/recibo/';
        var REST_INDEX_SERVICE_URI = '../GnklReporteador/index/cedis';
        
        var factory = {
            getAllProveedor: getAllProveedor,
            getAllClavesByProveedor: getAllClavesByProveedor,
            getAllClaves: getAllClaves,
            getAllProveedorByName: getAllProveedorByName,
            getSearchDataByParams: getSearchDataByParams,
            getSummaryByParams:getSummaryByParams,
            getReporteByParams:getReporteByParams,
            setCedisData:setCedisData
        };

        return factory;
        
        function getSearchDataByParams(idProveedor, idClave, parameterList, fechaInicio, fechaFin, page, offset) {          
            var url = REST_RECIBO_SERVICE_URI+'searchByParams';
            var deferred = $q.defer();
            var parameters = [];
            for(var i=0;i<parameterList.length;i++){
               var obj= parameterList[i];
               parameters.push(obj.column);
            }
            console.log(parameters);
            $http({
                method: 'GET',
                url: url,
                params:{
                    'idProveedor':idProveedor, 
                    'idClave':idClave, 
                    'parametros':parameters,
                    'fechaInicio':fechaInicio,
                    'fechaFin':fechaFin,
                    'page':page,
                    'offset':offset
                }
            })
                .then(
                function (response) {
                    deferred.resolve(response);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }        
        
        function getSummaryByParams(idProveedor, idClave, parameterList, fechaInicio, fechaFin) {          
            var url = REST_RECIBO_SERVICE_URI+'getSummatoryByParams';
            var deferred = $q.defer();
            var parameters = [];
            for(var i=0;i<parameterList.length;i++){
               var obj= parameterList[i];
               parameters.push(obj.column);
            }
            console.log(parameters);
            $http({
                method: 'GET',
                url: url,
                params:{
                    'idProveedor':idProveedor, 
                    'idClave':idClave, 
                    'parametros':parameters,
                    'fechaInicio':fechaInicio,
                    'fechaFin':fechaFin
                }
            })
                .then(
                function (response) {
                    deferred.resolve(response);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }        
        
        function getAllProveedor() {
            var url = REST_RECIBO_SERVICE_URI+'proveedores';
            var deferred = $q.defer();
            $http({method: 'GET',url: url})
                .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }

        function getAllProveedorByName(name) {
            var url = REST_RECIBO_SERVICE_URI+'proveedoresbyname';
            var deferred = $q.defer();
            $http({method: 'GET',
                url: url, 
                params:{'name':name}})
                .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }        
        
        function getAllClavesByProveedor(idProveedor){
            var url = REST_RECIBO_SERVICE_URI+'clavesbyproveedor';
            var deferred = $q.defer();
            $http({method: 'GET',
                url: url, 
                params:{'proveedor':idProveedor}})
                .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }
        
        function getAllClaves(){
            var url = REST_RECIBO_SERVICE_URI+'allclaves';
            var deferred = $q.defer();
            $http({method: 'GET',
                url: url
                }).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }
        
        function getReporteByParams(idProveedor, idClave, parameterList, fechaInicio, fechaFin) {          
            var url = REST_RECIBO_SERVICE_URI+'getReporteByParameters';
            var deferred = $q.defer();
            var parameters = [];
            for(var i=0;i<parameterList.length;i++){
               var obj= parameterList[i];
               parameters.push(obj.column);
            }
            //$http.get(url, {responseType: 'arraybuffer',params:{'tipoEnvio':$scope.tipoEnvio,'listStatus':$scope.estatusRow, 'filename':fileName}})
            console.log(parameters);
            $http({
                method: 'GET',
                url: url,
                responseType: 'arraybuffer',
                params:{
                    'idProveedor':idProveedor, 
                    'idClave':idClave, 
                    'parametros':parameters,
                    'fechaInicio':fechaInicio,
                    'fechaFin':fechaFin
                }
            }).then(
                function (response) {
                    deferred.resolve(response);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;
        }
        
        function setCedisData(cedis){
            var url = REST_INDEX_SERVICE_URI;
            var deferred = $q.defer();
            $http({method: 'GET',
                url: url,
                params:{
                    'cedis':cedis,
                }
                }).then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }
            );
            return deferred.promise;            
        }         
        
}]);

