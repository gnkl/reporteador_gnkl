/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
 
angular.module('reporteador').controller('CatalogoController', ['$scope', '$http', '$location','$q', '$rootScope', '$timeout','$filter', 'CatalogoService', function($scope, $http, $location,$q, $rootScope,$timeout,$filter, CatalogoService) {
    var self = this;
    self.tipoMedicamento=null;
    self.tipoUnidad=null;
    self.cedis=null;
    
    $scope.listTipo=[];
    $scope.listTipoUnidad=[];
    $scope.alltipos=null;
    
    self.selectedMedicamentoColumns=[];
    self.selectedUnidadColumns=[];
    
    $scope.cedisParameters = [
        {'desc':'Sendero','column':1},
        {'desc':'Lerma','column':0}
    ];    
    
    $scope.columnMedicamentoParameters=[
        {'desc':'Descripción','column':'F_DesPro'},
        {'desc':'Costo','column':'F_Costo'},
        {'desc':'Catálogo','column':'catalogos'},
        {'desc':'Distribuidor anterior','column':'distribuidores_anteriores'}        
    ];
    
    $scope.columnUnidadParameters=[
        {'desc':'Nombre','column':'F_NomCli'},
        {'desc':'Dirección','column':'F_Direc'},
        //{'desc':'Clave jurisdicción','column':'F_ClaJur'}
    ];
    $scope.tableMedicaHeader=[];

    self.selectedUnidadColumns=[];
    
    $scope.tableUnidadHeader=[];    
    
    $scope.tableMedicamentoData=[];
    $scope.tableUnidadData=[];
        
    $scope.itemsByPage = "10";
    $scope.listDispensadores=[];
    
    self.selectedColumn=[];
    self.totalUnidadQuery=0;
    
    self.selectedCedis=null;
    self.enableFormUnidad = false;
    
    $scope.tablestateMedica=null;
    $scope.tablestateUnidad=null;
    
    
    $scope.onSelectTipoMedicamentoCallback=function($item, $model){
        if($item!=null && $item!=undefined){
            self.tipoMedicamento = $item;
        }
    }
    
    $scope.getListTipo=function(){
        $scope.startDialogAjaxRequest();
        CatalogoService.getAllTipoMedicamento()
            .then(
            function(d) {
                $scope.listTipo = d.data;
                $scope.finishAjaxCall();
            },
            function(errResponse){
                console.error('Error while fetching Users');
                $scope.finishAjaxCall();
            }
        );                
    }
    
    $scope.searchMedicamentoDataByParams=function(form){
        if (!form.$valid) {
            $scope.displayValidationError = true;
            //return;
        }
        
        console.log('a ver');   
        //$scope.startDialogAjaxRequest();
        
        var start = 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = 10;  // Number of entries showed per page.
        var page=0;        

        $scope.tableMedicaHeader = [];
        $scope.tableMedicaHeader = [{'desc':'Clave','column':'F_ClaPro'}];
        
        if(self.selectedMedicamentoColumns!=undefined){
            $scope.tableMedicaHeader = $scope.tableMedicaHeader.concat(self.selectedMedicamentoColumns);
        }
        
        if($scope.tableMedicaHeader!=null && $scope.tableMedicaHeader!=undefined && $scope.tableMedicaHeader.length<2){
            alert('Seleccione algun parámetro a ser consultado');
            return;
        }
        
        if($scope.tablestateMedica!==null && $scope.tablestateMedica.pagination!==undefined){
            var pagination = $scope.tablestateMedica.pagination;
            start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            number = pagination.number || 10;  // Number of entries showed per page.            
            page=Math.floor(start / number);
        }
        
        
        var tipo = (self.tipoMedicamento!=null && self.tipoMedicamento!=undefined)?self.tipoMedicamento:null;
        
        CatalogoService.getSearchDataByParams(tipo, $scope.tableMedicaHeader, page, $scope.itemsByPage).then(
                function(response) {
//                    console.log(response);
                    $scope.tableMedicamentoData = response.data.data;
                    $scope.totalQueryResult=response.data.totalData;
//                    console.log($scope.tablestate);
                    if($scope.tablestateMedica.pagination!=null && $scope.tablestateMedica.pagination!=undefined){
                        $scope.tablestateMedica.pagination.numberOfPages = $scope.getNumberOfPages(response.data.totalData);
                    }
                    $scope.finishAjaxCall();
                    
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }
        )
        
        CatalogoService.getSummaryByParams(tipo, $scope.tableMedicaHeader).then(
                function(response) {
                    $scope.listSummaryData=response.data.data;
                    if($scope.listSummaryData!=null && $scope.listSummaryData.length>0){
                        $scope.summaryObj = $scope.listSummaryData[0];
                    }
                    $scope.finishAjaxCall();
                    
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }
        )        
        
    }
    
    $scope.serverMedicamentoBusquedaFilter = function(tablestate) {
        $scope.tablestateMedica=tablestate;
        if(self.selectedMedicamentoColumns!=null && self.selectedMedicamentoColumns!=undefined && self.selectedMedicamentoColumns.length>1){
            $scope.searchMedicamentoDataByParams('catalogoForm');
        }
    }    

    $scope.serverUnidadBusquedaFilter = function(tablestate) {
        $scope.tablestateUnidad=tablestate;
        if(self.selectedUnidadColumns!=null && self.selectedUnidadColumns!=undefined && self.selectedUnidadColumns.length>1){
            $scope.searchUnidadDataByParams('unidadesForm');
        }
    }
    
    $scope.searchUnidadDataByParams=function(form){
        if (!form.$valid) {
            $scope.displayValidationError = true;
            //return;
        }
        
        //$scope.startDialogAjaxRequest();
        var start = 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = 10;  // Number of entries showed per page.
        var page=0;        

        $scope.tableUnidadHeader = [];
        $scope.tableUnidadHeader = [{'desc':'Clave','column':'F_ClaCli'}];
        
        if(self.selectedUnidadColumns!=undefined){
            $scope.tableUnidadHeader = $scope.tableUnidadHeader.concat(self.selectedUnidadColumns);
        }
        
        if($scope.tableUnidadHeader!=null && $scope.tableUnidadHeader!=undefined && $scope.tableUnidadHeader.length<2){
            alert('Seleccione algun parámetro a ser consultado');
            return;
        }
        
        if($scope.tablestateUnidad!==null && $scope.tablestateUnidad.pagination!==undefined){
            var pagination = $scope.tablestateUnidad.pagination;
            start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            number = pagination.number || 10;  // Number of entries showed per page.            
            page=Math.floor(start / number);
        }
        
        
        var tipo = (self.tipoUnidad!=null && self.tipoUnidad!=undefined)?self.tipoUnidad.clave:'';
        
        CatalogoService.getSearchUnidadDataByParams(tipo, null, $scope.tableUnidadHeader, page, $scope.itemsByPage).then(
                function(response) {
//                    console.log(response);
                    $scope.tableUnidadData = response.data.data;
                    self.totalUnidadQuery=response.data.totalData;
//                    console.log($scope.tablestate);
                    if($scope.tablestateUnidad.pagination!=null && $scope.tablestateUnidad.pagination!=undefined){
                        $scope.tablestateUnidad.pagination.numberOfPages = $scope.getNumberOfPages(response.data.totalData);
                    }
                    $scope.finishAjaxCall();
                    
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }
        )
        
//        CatalogoService.getSummaryUnidadByParams(tipo, null, $scope.tableMedicaHeader).then(
//                function(response) {
//                    $scope.listSummaryData=response.data.data;
//                    if($scope.listSummaryData!=null && $scope.listSummaryData.length>0){
//                        $scope.summaryObj = $scope.listSummaryData[0];
//                    }
//                    $scope.finishAjaxCall();
//                    
//                },
//                function(errResponse){
//                    console.error('Error while fetching claves por unidad medica');
//                    $scope.finishAjaxCall();
//                }
//        )        
        
    }    
        
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
    }    
    
    $scope.finishAjaxCall = function () {
        $("#loadingModal").modal('hide');
        $scope.lastAction = '';
    }      
    
    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    };       
    
    $scope.onSelectTipoUnidadCallback=function($item, $model){
        if($item!=null && $item!=undefined){
            self.tipoUnidad = $item;
        }        
    }
    
    $scope.getListTipoUnidad=function(){
        $scope.startDialogAjaxRequest();
        CatalogoService.getAllTipoUnidad()
            .then(
            function(d) {
                $scope.listTipoUnidad = d.data;
                $scope.finishAjaxCall();
            },
            function(errResponse){
                console.error('Error while fetching Users');
                $scope.finishAjaxCall();
            }
        );                
    }    
    

//    $scope.getListDispensadores=function(){
//        $scope.startDialogAjaxRequest();
//        CatalogoService.getListDispensadores()
//            .then(
//            function(d) {
//                $scope.listDispensadores = d.data;
//                $scope.finishAjaxCall();
//            },
//            function(errResponse){
//                console.error('Error while fetching Users');
//                $scope.finishAjaxCall();
//            }
//        );                
//    }    
    
    $scope.clearDataUnidad=function(){
        self.tipoUnidad=null;
        self.selectedUnidadColumns=[];
        self.totalUnidadQuery=null;
        $scope.tableUnidadData=[];
        self.selectedCedis=null;
        self.selectedCedis=undefined;
        self.enableFormUnidad = false;
        $scope.totalQueryResult=null;
        $scope.summaryObj=[];
        $scope.tablestateUnidad.pagination.numberOfPages = $scope.getNumberOfPages(0);
    }
    
    $scope.clearDataByParams=function(){
        self.selectedMedicamentoColumns=undefined;
        self.tipoMedicamento=undefined;
        $scope.tableMedicamentoData=[];
        self.selectedCedis=null;
        self.selectedCedis=undefined;
        self.enableFormUnidad = false;        
        $scope.totalQueryResult=null;
        $scope.summaryObj=[];
        $scope.tablestateMedica.pagination.numberOfPages = $scope.getNumberOfPages(0);
    }
    
    $scope.eventChangeAllTipoMedicamentoClick=function(alltipos){
        if(alltipos==true){
            self.tipoMedicamento=undefined;        
        }
    }
    
    $scope.eventChangeAllTipoUnidadClick=function(alltipounidad){
        if(alltipounidad==true){
            self.tipoUnidad=undefined;
        }
    }
    
    $scope.onSelectCedisCallback = function($item, $model){
        if($item!=null && $item!=undefined){
            $scope.clearDataUnidad();
            $scope.clearDataByParams();
            
            self.enableFormUnidad = true;
            $scope.selectedCedis=$item;
            
            switch($item.column){
                case 0:
                    $scope.columnMedicamentoParameters=[
                        {'desc':'Descripción','column':'F_DesPro'},
                        {'desc':'Costo','column':'F_Costo'},
                        {'desc':'Catálogo','column':'catalogos'},
                        {'desc':'Distribuidor anterior','column':'distribuidores_anteriores'}        
                    ];                    
                break; 
                case 1:
                    $scope.columnMedicamentoParameters=[
                        {'desc':'Descripción','column':'F_DesPro'},
                        {'desc':'Costo','column':'F_Costo'}
                    ];                    
                break;
            }
            
            CatalogoService.setCedisData($item.column)
                .then(
                function(d) {
                    console.log(d);
                    $scope.getListTipoUnidad();
                    $scope.getListTipo();
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    $scope.finishAjaxCall();
                }
            );            
        }
    }
    
    $scope.createReporteMedicamento = function(fileName){
        //$scope.startDialogAjaxRequest();
        var now =  $filter('date')(new Date(),'yyyy-MM-dd');
        var namefile = fileName+'_Medicamento_'+now+".zip";
        var typefile='application/octet-stream';
        var url='';
        
        $scope.tableMedicaHeader = [];
        $scope.tableMedicaHeader = [{'desc':'Clave','column':'F_ClaPro'}];
        
        if(self.selectedMedicamentoColumns!=undefined){
            $scope.tableMedicaHeader = $scope.tableMedicaHeader.concat(self.selectedMedicamentoColumns);
        }        
        
        var tipo = (self.tipoMedicamento!=null && self.tipoMedicamento!=undefined)?self.tipoMedicamento:null;
                
        if($scope.tableMedicaHeader!=null && $scope.tableMedicaHeader!=undefined && $scope.tableMedicaHeader.length>1 ) {
            $scope.startDialogAjaxRequest();
            CatalogoService.getReporteMedicaByParams(tipo, $scope.tableMedicaHeader ).then(
                function(response) {
                    console.log(response);
                    var data = response.data;
                    $timeout(function(){  
                    var file = new Blob([data], {type: typefile});
                    var fileURL = URL.createObjectURL(file);
                    var a         = document.createElement('a');
                    a.href        = fileURL; 
                    a.target      = '_blank';
                    a.download    = namefile;
                    document.body.appendChild(a);
                    a.click();
                    });
                    $scope.finishAjaxCall();                    
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }                    
            )
        }        
        
        
    }

    $scope.createReporteUnidad = function(fileName){
        //$scope.startDialogAjaxRequest();
        var now =  $filter('date')(new Date(),'yyyy-MM-dd');
        var namefile = fileName+'_Unidades_'+now+".zip";
        var typefile='application/octet-stream';
        var url='';
        var tipo = (self.tipoUnidad!=null && self.tipoUnidad!=undefined)?self.tipoUnidad.clave:'';
        
        $scope.tableUnidadHeader = [];
        $scope.tableUnidadHeader = [{'desc':'Clave','column':'F_ClaCli'}];
        
        if(self.selectedUnidadColumns!=undefined){
            $scope.tableUnidadHeader = $scope.tableUnidadHeader.concat(self.selectedUnidadColumns);
        }
        
        //console.log(self.selectedUnidadColumns);

        if(self.selectedUnidadColumns!=null && self.selectedUnidadColumns!=undefined && self.selectedUnidadColumns.length>0 ){        
            $scope.startDialogAjaxRequest();
            CatalogoService.getReporteUnidadByParams(tipo, $scope.tableUnidadHeader ).then(
                function(response) {
                    console.log(response);
                    var data = response.data;
                    $timeout(function(){  
                    var file = new Blob([data], {type: typefile});
                    var fileURL = URL.createObjectURL(file);
                    var a         = document.createElement('a');
                    a.href        = fileURL; 
                    a.target      = '_blank';
                    a.download    = namefile;
                    document.body.appendChild(a);
                    a.click();
                    });
                    $scope.finishAjaxCall();                    
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }                    
            )
        }        
        
        
    }
        
    
}]);