/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
 
angular.module('reporteador').controller('ReciboController', ['$scope', '$http', '$location','$q', '$rootScope', '$timeout','$filter', 'ReciboService', function($scope, $http, $location,$q, $rootScope,$timeout,$filter, ReciboService) {
    var self = this;
    $scope.listProveedor=null;
    $scope.listClaves=null;
    self.proveedor={};
    self.clave={};
    $scope.tipoReporte=null;
    $scope.selectedPieza=null;
    $scope.selectedMonto=null;
    $scope.selectedFolio=null;
    $scope.fechaInicio=null;
    $scope.fechaFin=null;
    $scope.allUnidades=null;

    $scope.columnParameters=[
                         {'desc':'Costo','column':'F_Costo'},
                         {'desc':'Folio','column':'F_ClaDoc'},
                         {'desc':'Cantidad compra','column':'F_CanCom'},
                         {'desc':'Compra total','column':'F_ComTot'},
                         {'desc':'ORI','column':'F_OrdCom'},
                         {'desc':'Cajas','column':'F_Cajas'},
                         {'desc':'Importe total','column':'F_ImpTo'}
                         ];        
                                                
    $scope.cedisParameters = [
        {'desc':'Sendero','column':1},
        {'desc':'Lerma','column':0}
    ];                         
                     
    self.enableFormRecibo = false;
    self.selectedCedis = null;
    
    self.selectedColumns=[];
    $scope.totalQueryResult=0;
    $scope.tableHeader =  [];
    $scope.tableData =  [];
    $scope.tableDataSafe =  [];
    $scope.listSummaryData = [];
    $scope.tablestate={};
    $scope.summaryObj={};
    $scope.itemsByPage = "10";
    
    $scope.getListProveedor=function($select) {
        if($select.search!==null && $select.search !== undefined && $select.search!=='' && $select.search.length>=4){
            $scope.startDialogAjaxRequest();
            ReciboService.getAllProveedorByName($select.search)
                .then(
                function(d) {
                    $scope.listProveedor = d.data;
                    $scope.finishAjaxCall();
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    $scope.finishAjaxCall();
                }
            );
        }
    }
    
    $scope.onSelectProveedorCallback=function($item, $model){
        if($item != null && $item != undefined){
            self.proveedor = $item;
            $scope.startDialogAjaxRequest();
            ReciboService.getAllClavesByProveedor($item.clave).then(
                function(d) {
                    $scope.listClaves = d.data;
                    $scope.finishAjaxCall();
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }                    
            )
        }
    }    
    
    $scope.onSelectClaveCallback=function($item, $model){
        if($item != null && $item != undefined){
            self.clave = $item;
            /*$scope.startDialogAjaxRequest();
            ReciboService.getAllClavesByProveedor($item.clave).then(
                function(d) {
                    $scope.listClaves = d.data;
                    $scope.finishAjaxCall();
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }                    
            )*/
        }
    }
    
    $scope.eventChangeAllUnidadesClick=function($event){
        if($event==true){
            self.proveedor={};
            self.clave={};            
            $scope.listProveedor = null;
            $scope.listClaves = null;
            $scope.startDialogAjaxRequest();
            ReciboService.getAllClaves().then(
                function(d) {
                    $scope.listClaves = d.data;
                    $scope.finishAjaxCall();
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }                    
            )
        }
    }
    
    
    $scope.searchDataByParams=function(form){
        if (!form.$valid) {
            $scope.displayValidationError = true;
            //return;
        }
        $scope.startDialogAjaxRequest();
        var start = 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = 10;  // Number of entries showed per page.
        var page=0;        

        $scope.tableHeader = [];
        $scope.tableHeader = [{'desc':'Clave','column':'F_ClaPro'}];
        
        $scope.tableHeader = $scope.tableHeader.concat(self.selectedColumns);

        /*for(var i=0;i<self.selectedColumns.length;i++){
            $scope.tableHeader.push(self.selectedColumns[i]);
        }*/
        
        if($scope.tablestate!==null && $scope.tablestate.pagination!==undefined){
            var pagination = $scope.tablestate.pagination;
            start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            number = pagination.number || 10;  // Number of entries showed per page.            
            page=Math.floor(start / number);
        }            
        
        var fechaInicial = $filter('date')($scope.fechaInicio,'yyyy-MM-dd HH:mm:ss');
        var fechaFinal = $filter('date')($scope.fechaFin,'yyyy-MM-dd HH:mm:ss');
        
        //console.log(fechaInicial);
        //console.log(fechaFinal);        
        //console.log(self.proveedor.clave+' '+self.clave.clave+' '+$scope.tableHeader+' '+fechaInicial+' '+fechaFinal+' '+page+' '+$scope.itemsByPage);
        
        //TODO falta poner la pagina actual
        ReciboService.getSearchDataByParams(self.proveedor.clave, self.clave.clave, $scope.tableHeader, fechaInicial, fechaFinal, page, $scope.itemsByPage).then(
                function(response) {
//                    console.log(response);
                    $scope.tableDataSafe = response.data.data;
                    $scope.tableData = [].concat($scope.tableDataSafe);
                    $scope.totalQueryResult=response.data.totalData;
//                    console.log($scope.tablestate);
                    if($scope.tablestate.pagination!=null && $scope.tablestate.pagination!=undefined){
                        $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.data.totalData);
                    }
                    $scope.finishAjaxCall();
                    
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }
        )
        
        ReciboService.getSummaryByParams(self.proveedor.clave, self.clave.clave,$scope.tableHeader, fechaInicial, fechaFinal).then(
                function(response) {
//                    console.log(response);
                    $scope.listSummaryData=response.data.data;
                    if($scope.listSummaryData!=null && $scope.listSummaryData.length>0){
                        $scope.summaryObj = $scope.listSummaryData[0];
                    }
                    $scope.finishAjaxCall();
                    
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }
        )
    }
    
    $scope.createReporteBitacoraStatus = function(fileName){
        //$scope.startDialogAjaxRequest();
        var now =  $filter('date')(new Date(),'yyyy-MM-dd');
        var namefile = fileName+now+".zip";
        var typefile='application/octet-stream';
        var url='';
        
        $scope.tableHeader = [];
        $scope.tableHeader = [{'desc':'Clave','column':'F_ClaPro'}];
        
        for(var i=0;i<self.selectedColumns.length;i++){
            $scope.tableHeader.push(self.selectedColumns[i]);
        }
        
        console.log(self.proveedor);
        console.log(self.selectedColumns);
        //if(self.proveedor!=null && self.proveedor!=undefined && self.selectedColumns!=null && self.selectedColumns!=undefined && self.selectedColumns.length>0 && $scope.fechaInicio!=null && $scope.fechaInicio!=undefined && $scope.fechaFin!=undefined && $scope.fechaFin!=undefined){
        if(self.proveedor!=null && self.proveedor!=undefined && self.selectedColumns!=null && self.selectedColumns!=undefined && self.selectedColumns.length>0 && $scope.fechaInicio!=null && $scope.fechaInicio!=undefined && $scope.fechaFin!=undefined && $scope.fechaFin!=undefined){        
            $scope.startDialogAjaxRequest();
            var fechaInicial = $filter('date')($scope.fechaInicio,'yyyy-MM-dd HH:mm:ss');
            var fechaFinal = $filter('date')($scope.fechaFin,'yyyy-MM-dd HH:mm:ss');            
            ReciboService.getReporteByParams(self.proveedor.clave, self.clave.clave, $scope.tableHeader, fechaInicial, fechaFinal).then(
                function(response) {
                    console.log(response);
                    var data = response.data;
                    $timeout(function(){  
                    var file = new Blob([data], {type: typefile});
                    var fileURL = URL.createObjectURL(file);
                    var a         = document.createElement('a');
                    a.href        = fileURL; 
                    a.target      = '_blank';
                    a.download    = namefile;
                    document.body.appendChild(a);
                    a.click();
                    });
                    $scope.finishAjaxCall();                    
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }                    
            )
        }        
        
    };
    
    $scope.serverBusquedaFilter = function(tablestate) {
        $scope.tablestate=tablestate;
        if(self.proveedor!=null && self.proveedor!=undefined && self.clave!=null && self.clave!=undefined && self.selectedColumns!=null && self.selectedColumns!=undefined && self.selectedColumns.length>0){
            $scope.searchDataByParams('reciboForm');
        }
    };      
    
    
    $scope.clearDataByParams=function(){
        $scope.tipoReporte=0;
        //self.proveedor={};
        //self.clave={};
        $scope.tipoReporte=null;
        $scope.selectedPieza=null;
        $scope.selectedMonto=null;
        $scope.selectedFolio=null;
        $scope.fechaInicio=null;
        $scope.fechaFin=null;
        self.selectedColumns=[];
        //self.proveedor=null;
        //self.clave=null;
        console.log(self.proveedor);
        self.proveedor.selected = undefined;
        self.enableFormRecibo = false;
        self.selectedCedis=null;
        self.selectedCedis=undefined;
        self.enableFormRecibo = false;
        $scope.summaryObj=[];
        $scope.totalQueryResult=null;
    }    
    
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
    }    
    
    $scope.finishAjaxCall = function () {
        $("#loadingModal").modal('hide');
        $scope.lastAction = '';
    }    
    
    $scope.open1 = function() {
      $scope.popup1.opened = true;
    };

    $scope.open2 = function() {
      $scope.popup2.opened = true;
    };
    
    $scope.popup1 = {
      opened: false
    };

    $scope.popup2 = {
      opened: false
    };    
    
    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    };      
    
    $scope.onSelectCedisCallback = function($item, $model){
        if($item!=null && $item!=undefined){
            $scope.clearDataByParams();
            self.enableFormRecibo = true;
            self.selectedCedis=$item;
            ReciboService.setCedisData($item.column)
                .then(
                function(d) {
                    console.log(d);
                    //$scope.getListTipoUnidad();
                    //$scope.getListTipo();
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    $scope.finishAjaxCall();
                }
            );            
        }
    }       
    
}]);