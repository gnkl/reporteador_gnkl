/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
 
angular.module('reporteador').controller('ReporterController', ['$scope', '$http', '$location','$q', '$rootScope', '$timeout','$filter', 'ReporterService', function($scope, $http, $location,$q, $rootScope,$timeout,$filter, ReporterService) {
    var self = this;
    self.listUnidadMedica=null;
    self.listClaves=null;
    self.unidadMedica={};
    self.clave={};
    $scope.tipoReporte=null;
    $scope.selectedPieza=null;
    $scope.selectedMonto=null;
    $scope.selectedFolio=null;
    $scope.fechaInicio=null;
    $scope.fechaFin=null;
    $scope.allUnidades=null;
    /*$scope.columnParameters=[{'desc':'Piezas','column':'F_CantSur'},
                             {'desc':'Monto','column':'F_Monto'},
                             {'desc':'Folio','column':'F_ClaDoc'},
                             {'desc':'Solicitado','column':'F_CantReq'},
                             {'desc':'Surtido','column':'F_CantSur'}];*/
    $scope.columnParameters=[
                             {'desc':'Monto','column':'F_Monto'},
                             {'desc':'Folio','column':'F_ClaDoc'},
                             {'desc':'Solicitado','column':'F_CantReq'},
                             {'desc':'Surtido','column':'F_CantSur'}
                             ];  
                             
    $scope.cedisParameters = [
        {'desc':'Sendero','column':1},
        {'desc':'Lerma','column':0}
    ];                              
                             
    self.selectedColumns=[];
    $scope.totalQueryResult=0;
    $scope.tableHeader =  [];
    $scope.tableData =  [];
    $scope.tableDataSafe =  [];
    $scope.listSummaryData = [];
    $scope.tablestate={};
    $scope.summaryObj={};
    $scope.itemsByPage = "10";
    
    self.selectedCedis=null;
    self.enableFormEntrega = false;    
    
    
    self.getListUnidad = getListUnidad;
    self.onSelectUnidadCallback = onSelectUnidadCallback;
    
    
    function getAllUnidadMedica(){
        ReporterService.getAllUnidadMedica()
            .then(
            function(d) {
                self.listUnidadMedica = d.data;
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
        
    }
    
    function getListUnidad($select) {
        if($select.search!==null && $select.search !== undefined && $select.search!=='' && $select.search.length>=4){
            $scope.startDialogAjaxRequest();
            ReporterService.getAllUnidadMedicaByName($select.search)
                .then(
                function(d) {
                    self.listUnidadMedica = d.data;
                    $scope.finishAjaxCall();
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    $scope.finishAjaxCall();
                }
            );
        }
    }
    
    function onSelectUnidadCallback($item, $model){
        if($item != null && $item != undefined){
            self.unidadMedica = $item;
            $scope.startDialogAjaxRequest();
            ReporterService.getAllClaveByUnidadMedica($item.clave).then(
                function(d) {
                    self.listClaves = d.data;
                    $scope.finishAjaxCall();
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }                    
            )
        }
    }
    
    $scope.open1 = function() {
      $scope.popup1.opened = true;
    };

    $scope.open2 = function() {
      $scope.popup2.opened = true;
    };
    
    $scope.popup1 = {
      opened: false
    };

    $scope.popup2 = {
      opened: false
    };    
    
   
    
    $scope.clearDataByParams=function(){
        $scope.tipoReporte=0;
        self.unidadMedica={};
        self.clave={};
        $scope.tipoReporte=null;
        $scope.selectedPieza=null;
        $scope.selectedMonto=null;
        $scope.selectedFolio=null;
        $scope.fechaInicio=null;
        $scope.fechaFin=null;
        self.selectedColumns=[];
        self.enableFormEntrega = false;
        self.selectedCedis=null;     
        self.selectedCedis=undefined;
        $scope.summaryObj=[];
        $scope.totalQueryResult=null;        
    }
    
    
    $scope.eventChangeAllUnidadesClick=function($event){
        if($event==true){
            self.unidadMedica={};
            self.clave={};            
            self.listUnidadMedica = null;
            self.listClaves = null;
            $scope.startDialogAjaxRequest();
            ReporterService.getAllClaves().then(
                function(d) {
                    self.listClaves = d.data;
                    $scope.finishAjaxCall();
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }                    
            )
        }
    }
    
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
    }    
    
    $scope.finishAjaxCall = function () {
        $("#loadingModal").modal('hide');
        $scope.lastAction = '';
    }
    
  
  /*$scope.tableHeader =  ['number1','number2','number3'];     
  $scope.tableData =  [{number1:10,number2:8,number3:40},
                       {number1:1,number2:4,number3:55},
                       {number1:8,number2:10,number3:35},
                       {number1:8,number2:11,number3:44}];*/

    $scope.getRecordValue = function (property){
      return function (item){
        return item.dyn_array[property];
      }
    }
    
    $scope.serverBusquedaFilter = function(tablestate) {
        $scope.tablestate=tablestate;
        if(self.unidadMedica!=null && self.unidadMedica!=undefined && self.clave!=null && self.clave!=undefined && self.selectedColumns!=null && self.selectedColumns!=undefined && self.selectedColumns.length>0){
            $scope.searchDataByParams('reporterForm');
        }
    };  
    
    $scope.searchDataByParams=function(form){
        if (!form.$valid) {
            $scope.displayValidationError = true;
            //return;
        }
        $scope.startDialogAjaxRequest();
        var start = 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = 10;  // Number of entries showed per page.
        var page=0;        

        $scope.tableHeader = [];
        $scope.tableHeader = [{'desc':'Clave','column':'F_ClaPro'}];
        for(var i=0;i<self.selectedColumns.length;i++){
            $scope.tableHeader.push(self.selectedColumns[i]);
        }        
        
        if($scope.tablestate!==null && $scope.tablestate.pagination!==undefined){
            var pagination = $scope.tablestate.pagination;
            start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            number = pagination.number || 10;  // Number of entries showed per page.            
            page=Math.floor(start / number);
        }            
        
        var fechaInicial = $filter('date')($scope.fechaInicio,'yyyy-MM-dd HH:mm:ss');
        var fechaFinal = $filter('date')($scope.fechaFin,'yyyy-MM-dd HH:mm:ss');
        //TODO falta poner la pagina actual
        ReporterService.getSearchDataByParams(self.unidadMedica.clave, self.clave.clave, $scope.tableHeader, fechaInicial, fechaFinal, page, $scope.itemsByPage).then(
                function(response) {
//                    console.log(response);
                    $scope.tableDataSafe = response.data.data;
                    $scope.tableData = [].concat($scope.tableDataSafe);
                    $scope.totalQueryResult=response.data.totalData;
//                    console.log($scope.tablestate);
                    if($scope.tablestate.pagination!=null && $scope.tablestate.pagination!=undefined){
                        $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.data.totalData);
                    }
                    $scope.finishAjaxCall();
                    
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }
        )
        ReporterService.getSummaryByParams(self.unidadMedica.clave, self.clave.clave,$scope.tableHeader, fechaInicial, fechaFinal).then(
                function(response) {
//                    console.log(response);
                    $scope.listSummaryData=response.data.data;
                    if($scope.listSummaryData!=null && $scope.listSummaryData.length>0){
                        $scope.summaryObj = $scope.listSummaryData[0];
                    }
                    $scope.finishAjaxCall();
                    
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    $scope.finishAjaxCall();
                }
        )
    }
    
    $scope.createReporteBitacoraStatus = function(fileName){
        //$scope.startDialogAjaxRequest();
        var now =  $filter('date')(new Date(),'yyyy-MM-dd');
        var namefile = fileName+now+".zip";
        var typefile='application/octet-stream';
        var url='';
        
        $scope.tableHeader = [];
        $scope.tableHeader = [{'desc':'Clave','column':'F_ClaPro'}];
        for(var i=0;i<self.selectedColumns.length;i++){
            $scope.tableHeader.push(self.selectedColumns[i]);
        }
        
        
        if(self.unidadMedica!=null && self.unidadMedica!=undefined && self.selectedColumns!=null && self.selectedColumns!=undefined && self.selectedColumns.length>0 && $scope.fechaInicio!=null && $scope.fechaInicio!=undefined && $scope.fechaFin!=undefined && $scope.fechaFin!=undefined){
            $scope.startDialogAjaxRequest();
            var fechaInicial = $filter('date')($scope.fechaInicio,'yyyy-MM-dd HH:mm:ss');
            var fechaFinal = $filter('date')($scope.fechaFin,'yyyy-MM-dd HH:mm:ss');            
            ReporterService.getReporteByParams(self.unidadMedica.clave, self.clave.clave, $scope.tableHeader, fechaInicial, fechaFinal).then(
                function(response) {
                    console.log(response);
                    var data = response.data;
                    $timeout(function(){  
                    var file = new Blob([data], {type: typefile});
                    var fileURL = URL.createObjectURL(file);
                    var a         = document.createElement('a');
                    a.href        = fileURL; 
                    a.target      = '_blank';
                    a.download    = namefile;
                    document.body.appendChild(a);
                    a.click();
                    });
                    $scope.finishAjaxCall();                    
                },
                function(errResponse){
                    console.error('Error while fetching claves por unidad medica');
                    alert('Error while fetching data recibo');
                    $scope.finishAjaxCall();
                }                    
            )
        }        
        
    };     
    
    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    };    
   
    $scope.onSelectCedisCallback = function($item, $model){
        if($item!=null && $item!=undefined){
            $scope.clearDataByParams();
            self.enableFormEntrega = true;
            self.selectedCedis=$item;
            ReporterService.setCedisData($item.column)
                .then(
                function(d) {
                    console.log(d);
                    //$scope.getListTipoUnidad();
                    //$scope.getListTipo();
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    $scope.finishAjaxCall();
                }
            );            
        }
    }   
    
}]);

