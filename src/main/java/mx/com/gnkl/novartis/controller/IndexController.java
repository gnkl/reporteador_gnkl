/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.com.gnkl.novartis.bean.ObjectBeanVO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author jmejia
 */
@Controller
public class IndexController {
    
    public static Integer CURRENT_CEDIS=0; //0 LERMA//1 SENDERO    
    
    @RequestMapping(value = { "/", "/home" }, method = RequestMethod.GET)
    public String homePage(ModelMap model) {
        model.addAttribute("greeting", "Hi, Welcome to mysite. ");
        return "login";
    }
 
    @RequestMapping(value = "/reporter", method = RequestMethod.GET)
    public String reporteEntregaPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "reportEntregaPage";
    }

    @RequestMapping(value = "/recibo", method = RequestMethod.GET)
    public String reporteReciboPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "reportReciboPage";
    }

    @RequestMapping(value = "/catalogo", method = RequestMethod.GET)
    public String reporteCatalogoPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "reportCatalogoPage";
    }
    
    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String welcomePage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "welcomePage";
    }    
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "login";
    }
    
    @RequestMapping(value = "/accessdenied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "accessdenied";
    }    
    
    @RequestMapping(value="/logout", method = RequestMethod.GET)
       public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
          Authentication auth = SecurityContextHolder.getContext().getAuthentication();
          if (auth != null){    
             new SecurityContextLogoutHandler().logout(request, response, auth);
          }
          return "login";
       }    
    
    private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
 
        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }    
    
    @RequestMapping(value = "/index/cedis", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> setCurrentCedis(@RequestParam Integer cedis){
        CURRENT_CEDIS = cedis;
        ObjectBeanVO<Integer> cedisVO = new ObjectBeanVO<Integer>();
        cedisVO.setData(CURRENT_CEDIS);
        return new ResponseEntity<ObjectBeanVO<Integer>>(cedisVO, HttpStatus.OK);        
    }


}
