/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.repository.UnidadDao;
import mx.gnkl.process.file.ProcessExcelFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jmejia
 */
@RestController
@RequestMapping(value = "/unidad")
public class UnidadController {
    
    @Autowired
    private UnidadDao unidadLermaDao;    

    @Autowired
    private UnidadDao unidadSenderoDao;
    
    @Value("${reporte.catalogo.path}")
    private String pathReporte;
    
    @Autowired
    private ProcessExcelFile processExcel;    
    
    @RequestMapping(value = "/summatoryunidad", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?>  getSummatoryUnidadParams(@RequestParam(required = false, name="tipo") String tipo,
                                                       @RequestParam(required = false, name="municipio") Integer claveMunicipio,
                                                       @RequestParam(required = false, name="dispensador") Integer dispensador){
        ObjectListVO<Map<String, Object>> medicaListVO = new ObjectListVO<Map<String, Object>>();
        List<Map<String,Object>> listUnidad = new ArrayList<Map<String,Object>>();
        //List<Map<String,Object>> listUnidad = unidadSenderoDao.getAllSummatoryUnidadByParameters(tipo, claveMunicipio, dispensador);
        switch(IndexController.CURRENT_CEDIS){
            case 0:
                listUnidad = unidadLermaDao.getAllSummatoryUnidadByParameters(tipo, claveMunicipio, dispensador);
                break;
            case 1:
                listUnidad = unidadSenderoDao.getAllSummatoryUnidadByParameters(tipo, claveMunicipio, dispensador);
                break;
        }         
        medicaListVO.setData(listUnidad);
        medicaListVO.setTotalData(listUnidad.size());
        return new ResponseEntity<ObjectListVO<Map<String,Object>>>(medicaListVO, HttpStatus.OK);        
    }

    @RequestMapping(value = "/listunidad", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?>  getListUnidadByParams(@RequestParam String tipo,
                                                    @RequestParam(required = false, name="municipio") Integer claveMunicipio,
                                                    @RequestParam Integer dispensador, 
                                                    @RequestParam String[] parametros,
                                                    @RequestParam Integer page, Integer offset){
        ObjectListVO<Map<String, Object>> medicaListVO = new ObjectListVO<Map<String, Object>>();
        List<Map<String,Object>> listUnidad = new ArrayList<Map<String,Object>>();
        Long totalData = 0l;
        switch(IndexController.CURRENT_CEDIS){
            case 0:
                listUnidad = unidadLermaDao.getAllUnidadByParameters(tipo, claveMunicipio, dispensador, parametros, page, offset);
                totalData = unidadLermaDao.getTotalUnidadByParameters(tipo, claveMunicipio, dispensador);
                break;
            case 1:
                listUnidad = unidadSenderoDao.getAllUnidadByParameters(tipo, claveMunicipio, dispensador, parametros, page, offset);
                totalData = unidadSenderoDao.getTotalUnidadByParameters(tipo, claveMunicipio, dispensador);
                break;
        }        
        medicaListVO.setData(listUnidad);
        medicaListVO.setTotalData(totalData);
        return new ResponseEntity<ObjectListVO<Map<String,Object>>>(medicaListVO, HttpStatus.OK);        
    }    

    @RequestMapping(value = "/tipounidad", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?>  getTipoUnidadByParams(){
        ObjectListVO<Map<String, Object>> medicaListVO = new ObjectListVO<Map<String, Object>>();
        System.out.println(IndexController.CURRENT_CEDIS);
        List<Map<String,Object>> listUnidad = new ArrayList<Map<String,Object>>();
        switch(IndexController.CURRENT_CEDIS){
            case 0:
                listUnidad = unidadLermaDao.getAllTipoUnidad();
                break;
            case 1:
                listUnidad = unidadSenderoDao.getAllTipoUnidad();
                break;
        }
        medicaListVO.setData(listUnidad);
        medicaListVO.setTotalData(listUnidad.size());
        return new ResponseEntity<ObjectListVO<Map<String,Object>>>(medicaListVO, HttpStatus.OK);        
    }    
//
//    @RequestMapping(value = "/getalldispensadores", method = RequestMethod.GET, produces = "application/json")
//    public ResponseEntity<?>  getAllDispensadores(){
//        ObjectListVO<Map<String, Object>> medicaListVO = new ObjectListVO<Map<String, Object>>();
//        List<Map<String,Object>> listUnidad = unidadSenderoDao.getAllDispensadorList();
//        medicaListVO.setData(listUnidad);
//        medicaListVO.setTotalData(listUnidad.size());
//        return new ResponseEntity<ObjectListVO<Map<String,Object>>>(medicaListVO, HttpStatus.OK);        
//    }    
    
    @RequestMapping(value = "/getReporteByParameters", method = RequestMethod.GET, produces = "application/json")
    public byte[] getReporteByParameters(@RequestParam("tipo") String tipoUnidad, String[] parametros ) throws IOException, Exception {
        Map<Integer, String> mapHeader = new HashMap<Integer,String>();
        //DateFormat format = new SimpleDateFormat("yyyyMMdd");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
        String fecha = format.format(new Date());
        String fileName="Reporte_Unidad_"+tipoUnidad+"_"+fecha;
        
        for(int i=0;i<parametros.length;i++){
            System.out.println(i+" "+parametros[i]);
            mapHeader.put(i, parametros[i]);
        }
        File fileTemp = new File(pathReporte+fileName+".xlsx");
        if (fileTemp.exists()){
           fileTemp.delete();
        }         
        List<Map<String,Object>> listPrint = new ArrayList<Map<String,Object>>();
        //List<Map<String,Object>> listPrint = facturaSenderoDao.getReciboByParameters(idProveedor, idClave, parametros, fechaInicio, fechaFin);
       
        switch(IndexController.CURRENT_CEDIS){
            case 0:
                listPrint = unidadLermaDao.getAllUnidadByParameters(tipoUnidad, parametros);
                break;
            case 1:
                listPrint = unidadSenderoDao.getAllUnidadByParameters(tipoUnidad, parametros);
                break;
        }
        
        processExcel.writeCollection(pathReporte+fileName+".xlsx", "Detalle", listPrint, mapHeader);
        
        return getFileReporte(fileName);        
    }            
                
    //@Override
    public byte[] getFileReporte(String fileName) throws IOException {
            //pathOperador+fileName
            String fileZipName = String.valueOf(fileName + (new Date().getTime())) + ".zip";
            File zipfile = new File(pathReporte+fileZipName);
            // Create a buffer for reading the files
            byte[] buf = new byte[1024];
            // create the ZIP file
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipfile));
                Path pathFile = Paths.get(pathReporte+fileName+".xlsx");
                File file = pathFile.toFile();
                FileInputStream in = new FileInputStream(file);
                // add ZIP entry to output stream
                out.putNextEntry(new ZipEntry(file.getName()));
                // transfer bytes from the file to the ZIP file
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                // complete the entry
                out.closeEntry();
                in.close();
            out.close();
            byte[] data = Files.readAllBytes(zipfile.toPath());
            return data;       
    }        
    
}
