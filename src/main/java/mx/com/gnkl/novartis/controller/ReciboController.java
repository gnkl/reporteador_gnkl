/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.repository.FacturaDao;
import mx.com.gnkl.novartis.repository.ProveedorDao;
import mx.gnkl.process.file.ProcessExcelFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jmejia
 */
@RestController
@RequestMapping(value = "/recibo")
public class ReciboController {
    
    @Autowired
    private ProveedorDao proveedorSenderoDao;

    @Autowired
    private ProveedorDao proveedorLermaDao;    
    //@Autowired
    //@Qualifier("facturaLermaDao")
    //private FacturaDao facturaDao;   

    @Autowired
    private FacturaDao facturaLermaDao;   

    @Autowired
    private FacturaDao facturaSenderoDao;   
    
    @Value("${reporte.recibo.path}")
    private String pathReporte;
    
    @Autowired
    private ProcessExcelFile processExcel;           
    
    @RequestMapping(value = "/proveedores", method = RequestMethod.GET, produces = "application/json")
    public  ResponseEntity<?> getAllUnidades() {
        ObjectListVO<Map<String, Object>> bitacoraListVO = new ObjectListVO<Map<String, Object>>();
        List<Map<String,Object>> listProveedor = new ArrayList<Map<String,Object>>();
        switch(IndexController.CURRENT_CEDIS){
            case 0:
                listProveedor = proveedorLermaDao.getAllProveedor();
                break;
            case 1:
                listProveedor = proveedorSenderoDao.getAllProveedor();
                break;
        }         
        //List<Map<String,Object>> listProveedor = proveedorSenderoDao.getAllProveedor();
        bitacoraListVO.setData(listProveedor);
        bitacoraListVO.setTotalData(listProveedor.size());            
        return new ResponseEntity<ObjectListVO<Map<String,Object>>>(bitacoraListVO, HttpStatus.OK);
    }    

    @RequestMapping(value = "/proveedoresbyname", method = RequestMethod.GET, produces = "application/json")
    public  ResponseEntity<?> getAllUnidades(@RequestParam() String name) {
        ObjectListVO<Map<String, Object>> bitacoraListVO = new ObjectListVO<Map<String, Object>>();
        List<Map<String,Object>> listProveedor = new ArrayList<Map<String,Object>>();
        //List<Map<String,Object>> listProveedor = proveedorSenderoDao.getAllProveedorByName(name);
        switch(IndexController.CURRENT_CEDIS){
            case 0:
                listProveedor = proveedorLermaDao.getAllProveedorByName(name);
                break;
            case 1:
                listProveedor = proveedorSenderoDao.getAllProveedorByName(name);
                break;
        }
        bitacoraListVO.setData(listProveedor);
        bitacoraListVO.setTotalData(listProveedor.size());
        return new ResponseEntity<ObjectListVO<Map<String,Object>>>(bitacoraListVO, HttpStatus.OK);
    }     

    @RequestMapping(value = "/clavesbyproveedor", method = RequestMethod.GET, produces = "application/json")
    public  ResponseEntity<?> getAllClavesByProveedor(@RequestParam("proveedor") Integer idProveedor) {
        ObjectListVO<Map<String, Object>> bitacoraListVO = new ObjectListVO<Map<String, Object>>();
        List<Map<String,Object>> listProovedor = new ArrayList<Map<String,Object>>();
        //List<Map<String,Object>> listProovedor = proveedorSenderoDao.getAllClavesByProveedor(idProveedor);
        switch(IndexController.CURRENT_CEDIS){
            case 0:
                listProovedor = proveedorLermaDao.getAllClavesByProveedor(idProveedor);
                break;
            case 1:
                listProovedor = proveedorSenderoDao.getAllClavesByProveedor(idProveedor);
                break;
        }        
        bitacoraListVO.setData(listProovedor);
        bitacoraListVO.setTotalData(listProovedor.size());            
        return new ResponseEntity<ObjectListVO<Map<String,Object>>>(bitacoraListVO, HttpStatus.OK);
    }         
    
    @RequestMapping(value = "/allclaves", method = RequestMethod.GET, produces = "application/json")
    public  ResponseEntity<?> getAllClaves() {
        ObjectListVO<Map<String, Object>> bitacoraListVO = new ObjectListVO<Map<String, Object>>();
        List<Map<String,Object>> listProovedor = new ArrayList<Map<String,Object>>();
//        List<Map<String,Object>> listProovedor = proveedorSenderoDao.getAllClaves();
        switch(IndexController.CURRENT_CEDIS){
            case 0:
                listProovedor = proveedorLermaDao.getAllClaves();
                break;
            case 1:
                listProovedor = proveedorSenderoDao.getAllClaves();
                break;
        }
        bitacoraListVO.setData(listProovedor);
        bitacoraListVO.setTotalData(listProovedor.size());            
        return new ResponseEntity<ObjectListVO<Map<String,Object>>>(bitacoraListVO, HttpStatus.OK);
    }    

    @RequestMapping(value = "/searchByParams", method = RequestMethod.GET, produces = "application/json")
    public  ResponseEntity<?> getAllClaves(Integer idProveedor, String idClave, String[] parametros, String fechaInicio, String fechaFin, Integer page, Integer offset) {
        ObjectListVO<Map<String, Object>> bitacoraListVO = new ObjectListVO<Map<String, Object>>();
        List<Map<String,Object>> listFacturas = new ArrayList<Map<String,Object>>();
        Long total = 0l;
//        List<Map<String,Object>> listFacturas = facturaSenderoDao.getCompraByParameters(idProveedor, idClave, parametros, fechaInicio, fechaFin, page, offset);
//        Long total = facturaSenderoDao.getTotalCompraByParameters(idProveedor, idClave, fechaInicio, fechaFin);
        switch(IndexController.CURRENT_CEDIS){
            case 0:
                listFacturas = facturaLermaDao.getCompraByParameters(idProveedor, idClave, parametros, fechaInicio, fechaFin, page, offset);
                total = facturaLermaDao.getTotalCompraByParameters(idProveedor, idClave, fechaInicio, fechaFin);
                break;
            case 1:
                listFacturas = facturaSenderoDao.getCompraByParameters(idProveedor, idClave, parametros, fechaInicio, fechaFin, page, offset);
                total = facturaSenderoDao.getTotalCompraByParameters(idProveedor, idClave, fechaInicio, fechaFin);
                break;
        }
        bitacoraListVO.setData(listFacturas);
        bitacoraListVO.setTotalData(total);
        return new ResponseEntity<ObjectListVO<Map<String,Object>>>(bitacoraListVO, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getSummatoryByParams", method = RequestMethod.GET, produces = "application/json")
    public  ResponseEntity<?> getSummatoryByParameters(Integer idProveedor, String idClave, String[] parametros, String fechaInicio, String fechaFin) {
        ObjectListVO<Map<String, Object>> bitacoraListVO = new ObjectListVO<Map<String, Object>>();
        List<Map<String,Object>> listFacturas = new ArrayList<Map<String,Object>>();
//        List<Map<String,Object>> listFacturas = facturaSenderoDao.getSummatoryCompraByParameters(idProveedor, idClave, parametros, fechaInicio, fechaFin);
        switch(IndexController.CURRENT_CEDIS){
            case 0:
                listFacturas = facturaSenderoDao.getSummatoryCompraByParameters(idProveedor, idClave, parametros, fechaInicio, fechaFin);
                break;
            case 1:
                listFacturas = facturaSenderoDao.getSummatoryCompraByParameters(idProveedor, idClave, parametros, fechaInicio, fechaFin);
                break;
        }
        bitacoraListVO.setData(listFacturas);
        bitacoraListVO.setTotalData(listFacturas.size());
        return new ResponseEntity<ObjectListVO<Map<String,Object>>>(bitacoraListVO, HttpStatus.OK);
    }


    @RequestMapping(value = "/getReporteByParameters", method = RequestMethod.GET, produces = "application/json")
    public byte[] getReporteByParameters(Integer idProveedor, String idClave, String[] parametros, String fechaInicio, String fechaFin) throws IOException, Exception {
        
        Map<Integer, String> mapHeader = new HashMap<Integer,String>();
        //DateFormat format = new SimpleDateFormat("yyyyMMdd");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
        String fecha = format.format(new Date());
        String unidad = (idProveedor!=null)?idProveedor.toString():"All";
        String clave = (idClave!=null && !idClave.isEmpty())?idClave:"";
        String fileName="Reporte_Recibo_"+unidad+"_"+clave+"_"+fecha;
        
        for(int i=0;i<parametros.length;i++){
            System.out.println(i+" "+parametros[i]);
            mapHeader.put(i, parametros[i]);
        }
        File fileTemp = new File(pathReporte+fileName+".xlsx");
        if (fileTemp.exists()){
           fileTemp.delete();
        }         
        List<Map<String,Object>> listPrint = new ArrayList<Map<String,Object>>();
        //List<Map<String,Object>> listPrint = facturaSenderoDao.getReciboByParameters(idProveedor, idClave, parametros, fechaInicio, fechaFin);
        switch(IndexController.CURRENT_CEDIS){
            case 0:
                listPrint = facturaLermaDao.getReciboByParameters(idProveedor, idClave, parametros, fechaInicio, fechaFin);
                break;
            case 1:
                listPrint = facturaSenderoDao.getReciboByParameters(idProveedor, idClave, parametros, fechaInicio, fechaFin);
                break;
        }        
        processExcel.writeCollection(pathReporte+fileName+".xlsx", "Detalle", listPrint, mapHeader);
        
        return getFileReporte(fileName);
    }            
    
    //@Override
    public byte[] getFileReporte(String fileName) throws IOException {
            //pathOperador+fileName
            String fileZipName = String.valueOf(fileName + (new Date().getTime())) + ".zip";
            File zipfile = new File(pathReporte+fileZipName);
            // Create a buffer for reading the files
            byte[] buf = new byte[1024];
            // create the ZIP file
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipfile));
                Path pathFile = Paths.get(pathReporte+fileName+".xlsx");
                File file = pathFile.toFile();
                FileInputStream in = new FileInputStream(file);
                // add ZIP entry to output stream
                out.putNextEntry(new ZipEntry(file.getName()));
                // transfer bytes from the file to the ZIP file
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                // complete the entry
                out.closeEntry();
                in.close();
            out.close();
            byte[] data = Files.readAllBytes(zipfile.toPath());
            return data;       
    }    
}
