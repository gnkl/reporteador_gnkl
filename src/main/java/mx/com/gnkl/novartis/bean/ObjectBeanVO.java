package mx.com.gnkl.novartis.bean;

public class ObjectBeanVO <E> {
    private int pagesCount;
    private long totalData;

    private String actionMessage;
    private String searchMessage;

    private E data;

    public ObjectBeanVO() {
    }

    public ObjectBeanVO(int pages, long totalData, E data) {
        this.pagesCount = pages;
        this.data = data;
        this.totalData = totalData;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }

    public long getTotalData() {
        return totalData;
    }

    public void setTotalData(long totalContacts) {
        this.totalData = totalContacts;
    }

    public String getActionMessage() {
        return actionMessage;
    }

    public void setActionMessage(String actionMessage) {
        this.actionMessage = actionMessage;
    }

    public String getSearchMessage() {
        return searchMessage;
    }

    public void setSearchMessage(String searchMessage) {
        this.searchMessage = searchMessage;
    }

    /**
     * @return the data
     */
    public E getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(E data) {
        this.data = data;
    }
}