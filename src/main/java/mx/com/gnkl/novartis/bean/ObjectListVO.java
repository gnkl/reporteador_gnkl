package mx.com.gnkl.novartis.bean;

import java.util.List;

public class ObjectListVO <E> {
    private int pagesCount;
    private long totalData;

    private String actionMessage;
    private String searchMessage;

    private List<E> data;
    
    private Object extraParam ;

    public ObjectListVO() {
    }

    public ObjectListVO(int pages, long totalData, List<E> data) {
        this.pagesCount = pages;
        this.data = data;
        this.totalData = totalData;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }

    public long getTotalData() {
        return totalData;
    }

    public void setTotalData(long totalContacts) {
        this.totalData = totalContacts;
    }

    public String getActionMessage() {
        return actionMessage;
    }

    public void setActionMessage(String actionMessage) {
        this.actionMessage = actionMessage;
    }

    public String getSearchMessage() {
        return searchMessage;
    }

    public void setSearchMessage(String searchMessage) {
        this.searchMessage = searchMessage;
    }

    /**
     * @return the data
     */
    public List<E> getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(List<E> data) {
        this.data = data;
    }

    /**
     * @return the extraParam
     */
    public Object getExtraParam() {
        return extraParam;
    }

    /**
     * @param extraParam the extraParam to set
     */
    public void setExtraParam(Object extraParam) {
        this.extraParam = extraParam;
    }
}