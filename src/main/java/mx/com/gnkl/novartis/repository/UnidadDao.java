/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository;

import java.util.List;
import java.util.Map;

/**
 *
 * @author jmejia
 */
public interface UnidadDao {
    
    public List<Map<String,Object>> getAllUnidad();
    
    public List<Map<String,Object>> getAllUnidadByName(String name);
    
    public List<Map<String,Object>> getAllClavesByUnidad(String idUnidad);
    
    public List<Map<String,Object>> getAllClaves();
    
    public List<Map<String, Object>> getAllSummatoryUnidadByParameters(String tipo, Integer claveMunicipio, Integer dispensador);
    
    public List<Map<String,Object>> getAllUnidadByParameters(String tipo, Integer claveMunicipio, Integer dispensador, String[] parametros, Integer page, Integer offset);
    
    public Long getTotalUnidadByParameters(String tipo, Integer claveMunicipio, Integer dispensador);
    
    public List<Map<String, Object>> getAllTipoUnidad();
    
    public List<Map<String,Object>> getAllDispensadorList();
    
    public List<Map<String,Object>> getAllUnidadByParameters(String tipo, String[] parametros);    
    
}
