/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository;

import java.util.List;
import java.util.Map;

/**
 *
 * @author jmejia
 */
public interface ProveedorDao {
    
    public List<Map<String,Object>> getAllProveedor();
    
    public List<Map<String,Object>> getAllProveedorByName(String name);
    
    public List<Map<String,Object>> getAllClavesByProveedor(Integer idProveedor);
    
    public List<Map<String,Object>> getAllClaves();
    
}
