/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository;

import java.util.List;
import java.util.Map;

/**
 *
 * @author jmejia
 */
public interface MedicaDao {
    
    public List<Map<String, Object>> getSummaryMedicamentoByParameters(Integer tipoMedicamento, String[] parametros);

    public List<Map<String,Object>> getAllMedicamentoByParameters(Integer tipoMedicamento, String[] parametros, Integer page, Integer offset);
    
    public List<Map<String,Object>> getAllMedicamentoByParameters(Integer tipoMedicamento, String[] parametros);    
    
    public Long getTotalMedicamentoByParameters(Integer tipoMedicamento);    
    
    public List<Map<String,Object>> getAllTipoMedicamento();
    
        
}
