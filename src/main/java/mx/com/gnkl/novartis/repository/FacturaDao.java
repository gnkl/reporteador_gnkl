/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository;

import java.util.List;
import java.util.Map;

/**
 *
 * @author jmejia
 */
public interface FacturaDao {

    public List<Map<String,Object>> getFacturaByParameters(String idUnidad, String clave, String[] parametros, String fechaInicio, String fechaFin, Integer page, Integer offset);
    
    public Long getTotalFacturaByParameters(String idUnidad, String clave, String fechaInicio, String fechaFin);
    
    public List<Map<String, Object>> getSummatoryFacturaByParameters(String idUnidad, String clave, String[] parametros, String fechaInicio, String fechaFin);
    
    public List<Map<String, Object>> getFacturaByParameters(String idUnidad, String clave, String[] parametros, String fechaInicio, String fechaFin);
    
    //RECIBO
    
    public List<Map<String,Object>> getCompraByParameters(Integer idProveedor, String clave, String[] parametros, String fechaInicio, String fechaFin, Integer page, Integer offset);
    
    public Long getTotalCompraByParameters(Integer idProveedor, String clave, String fechaInicio, String fechaFin);
    
    public List<Map<String, Object>> getSummatoryCompraByParameters(Integer idProveedor, String clave, String[] parametros, String fechaInicio, String fechaFin);
    
    public List<Map<String, Object>> getReciboByParameters(Integer idProveedor, String clave, String[] parametros, String fechaInicio, String fechaFin);
}
