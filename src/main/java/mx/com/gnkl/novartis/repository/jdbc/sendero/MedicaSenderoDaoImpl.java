/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jdbc.sendero;

import java.util.List;
import java.util.Map;
import mx.com.gnkl.novartis.repository.MedicaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jmejia
 */
@Repository("medicaSenderoDao")
public class MedicaSenderoDaoImpl implements MedicaDao{
    
    @Autowired
    @Qualifier("jdbcTemplateSendero")
    private JdbcTemplate jdbcTemplate;
    
    @Override
    public List<Map<String, Object>> getSummaryMedicamentoByParameters(Integer tipoMedicamento, String[] parametros) {
        String query = " SELECT  ";
        
        if(parametros!=null && parametros.length>0){
            for(int i=0;i<parametros.length;i++){
                if(parametros[i]!=null){
                    switch(parametros[i]){
                        case "F_Costo":    
                            query+=" SUM(med."+parametros[i]+") "+parametros[i]+" ,";
                            break;
                        default:
                            break;
                    }
                    
                }
            }
        }
        query += " COUNT(DISTINCT(med.F_ClaPro)) TOTAL ";         
        
        //query += " FROM gnklmex_saa_lerma.tb_factura WHERE F_StsFact='A' ";
        query += " FROM tb_medica med  WHERE med.F_StsPro='A' ";
        
        if(tipoMedicamento!=null && tipoMedicamento>0){
            query += " AND med.F_TipMed = "+tipoMedicamento;
        }        
        
        System.out.println(query);
        //sqlParameterSource.addValue("page", 0);
        //sqlParameterSource.addValue("offset", 10);
        return jdbcTemplate.queryForList(query);          

    }

    @Override
    public List<Map<String, Object>> getAllMedicamentoByParameters(Integer tipoMedicamento, String[] parametros, Integer page, Integer offset) {
        String query = " SELECT ";
        if(parametros!=null && parametros.length>0){
            for(int i=0;i<parametros.length;i++){
                if(i==(parametros.length-1)){
                    query+=parametros[i];
                }else{
                    query+=parametros[i]+",";
                }
                
            }
        }

        query += " FROM tb_medica med WHERE med.F_StsPro='A' ";
        
        if(tipoMedicamento!=null && tipoMedicamento>0){
            query += " AND med.F_TipMed = "+tipoMedicamento;
        }
                
        query += " ORDER BY F_Costo DESC LIMIT "+(page*offset)+","+offset;
        System.out.println(query);
        return jdbcTemplate.queryForList(query);  
    }

    @Override
    public Long getTotalMedicamentoByParameters(Integer tipoMedicamento) {
        String query = " SELECT COUNT(DISTINCT(med.F_ClaPro)) TOTAL";

        query += " FROM tb_medica med WHERE med.F_StsPro='A' ";
        
        if(tipoMedicamento!=null && tipoMedicamento>0){
            query += " AND med.F_TipMed = "+tipoMedicamento;
        }
                
        System.out.println(query);
        return jdbcTemplate.queryForObject(query,Long.class);  
    }

    @Override
    public List<Map<String, Object>> getAllTipoMedicamento() {
        String query = " SELECT F_TipMed clave,F_DesMed nombre FROM tb_tipmed ";
        return jdbcTemplate.queryForList(query);  
    }

    @Override
    public List<Map<String, Object>> getAllMedicamentoByParameters(Integer tipoMedicamento, String[] parametros) {
        String query = " SELECT ";
        if(parametros!=null && parametros.length>0){
            for(int i=0;i<parametros.length;i++){
                if(i==(parametros.length-1)){
                    query+=parametros[i];
                }else{
                    query+=parametros[i]+",";
                }
                
            }
        }

        query += " FROM tb_medica med WHERE med.F_StsPro='A' ";
        
        if(tipoMedicamento!=null && tipoMedicamento>0){
            query += " AND med.F_TipMed = "+tipoMedicamento;
        }
                
        query += " ORDER BY F_Costo DESC ";
        System.out.println(query);
        return jdbcTemplate.queryForList(query);  

    }
    
}
