/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jdbc.sendero;

import java.util.List;
import java.util.Map;
import mx.com.gnkl.novartis.repository.FacturaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jmejia
 */
@Repository("facturaSenderoDao")
public class FacturaSenderoDaoImpl implements FacturaDao{
    @Autowired
    @Qualifier("jdbcTemplateSendero")
    private JdbcTemplate jdbcTemplate;
    
    @Override
    public List<Map<String, Object>> getFacturaByParameters(String idUnidad, String clave, String[] parametros, String fechaInicio, String fechaFin, Integer page, Integer offset) {
        String query = " SELECT ";
        if(parametros!=null && parametros.length>0){
            for(int i=0;i<parametros.length;i++){
                if(i==(parametros.length-1)){
                    query+=parametros[i];
                }else{
                    query+=parametros[i]+",";
                }
                
            }
        }

        query += " FROM tb_factura WHERE F_StsFact='A' ";
        if(idUnidad!=null && !idUnidad.isEmpty()){
            query += " AND F_ClaCli = '"+idUnidad+"' ";
            //sqlParameterSource.addValue("idUnidad", idUnidad);
            
        }
        if(clave!=null && !clave.isEmpty()){
            query += " AND F_ClaPro = '"+clave+"' ";
            //sqlParameterSource.addValue("clave", clave);
        }
        if(fechaInicio!=null && !fechaInicio.isEmpty() && fechaFin!=null && !fechaFin.isEmpty()){
            query += " AND date(F_FecEnt) >= date('"+fechaInicio+"') ";
            query += " AND date(F_FecEnt) <= date('"+fechaFin+"') ";
        }
        
        query += " LIMIT "+(page*offset)+","+offset;
        System.out.println(query);
        //sqlParameterSource.addValue("page", 0);
        //sqlParameterSource.addValue("offset", 10);
        return jdbcTemplate.queryForList(query);  
    }

    //TODO colocar algun paginador
    @Override
    public List<Map<String, Object>> getFacturaByParameters(String idUnidad, String clave, String[] parametros, String fechaInicio, String fechaFin) {
        String query = " SELECT ";
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        if(parametros!=null && parametros.length>0){
            for(int i=0;i<parametros.length;i++){
                if(i==(parametros.length-1)){
                    query+=parametros[i];
                }else{
                    query+=parametros[i]+",";
                }
                
            }
        }

        query += " FROM tb_factura f , tb_uniatn u  WHERE f.F_StsFact='A' AND u.F_Tipo != 'DIS' ";
        query += " AND f.F_ClaCli = u.F_ClaCli ";
        if(idUnidad!=null && !idUnidad.isEmpty()){
            query += " AND f.F_ClaCli = '"+idUnidad+"' ";
            
        }
        if(clave!=null && !clave.isEmpty()){
            query += " AND f.F_ClaPro = '"+clave+"' ";
            //sqlParameterSource.addValue("clave", clave);
        }
        if(fechaInicio!=null && !fechaInicio.isEmpty() && fechaFin!=null && !fechaFin.isEmpty()){
            query += " AND date(f.F_FecEnt) >= date('"+fechaInicio+"') ";
            query += " AND date(f.F_FecEnt) <= date('"+fechaFin+"') ";
        }
        
        System.out.println(query);
        //sqlParameterSource.addValue("page", 0);
        //sqlParameterSource.addValue("offset", 10);
        return jdbcTemplate.queryForList(query);  
    }
    
    @Override
    public List<Map<String, Object>> getSummatoryFacturaByParameters(String idUnidad, String clave, String[] parametros, String fechaInicio, String fechaFin) {
        String query = " SELECT ";
        
        if(parametros!=null && parametros.length>0){
            for(int i=0;i<parametros.length;i++){
                if(parametros[i]!=null){
                    switch(parametros[i]){
                        case "F_CantReq":
                        case "F_CantSur":    
                        case "F_Monto":    
                            query+=" SUM(f."+parametros[i]+")"+parametros[i]+" ,";
                            break;
                        case "F_ClaDoc":
                        case "F_ClaPro":    
                            query+=" COUNT(DISTINCT(f."+parametros[i]+")) "+parametros[i]+" ,";
                            break;
                        default:
                            break;
                    }
                    
                }
            }
        }
        query += " COUNT(DISTINCT(f.F_IdFact)) TOTAL ";        
        //query += " FROM gnklmex_saa_lerma.tb_factura WHERE F_StsFact='A' ";
        query += " FROM tb_factura f , tb_uniatn u WHERE f.F_StsFact='A' AND u.F_Tipo != 'DIS' ";
        query += " AND f.F_ClaCli = u.F_ClaCli ";
        
        if(idUnidad!=null && !idUnidad.isEmpty()){
            query += " AND f.F_ClaCli = '"+idUnidad+"' ";
            //sqlParameterSource.addValue("idUnidad", idUnidad);
            
        }
        if(clave!=null && !clave.isEmpty()){
            query += " AND f.F_ClaPro = '"+clave+"' ";
            //sqlParameterSource.addValue("clave", clave);
        }
        if(fechaInicio!=null && !fechaInicio.isEmpty() && fechaFin!=null && !fechaFin.isEmpty()){
            query += " AND date(f.F_FecEnt) >= date('"+fechaInicio+"') ";
            query += " AND date(f.F_FecEnt) <= date('"+fechaFin+"') ";
        }
        
        System.out.println(query);
        //sqlParameterSource.addValue("page", 0);
        //sqlParameterSource.addValue("offset", 10);
        return jdbcTemplate.queryForList(query);  
    }
    
    //TODO colocar algun paginador
    @Override
    public Long getTotalFacturaByParameters(String idUnidad, String clave, String fechaInicio, String fechaFin) {
        String query = " SELECT COUNT(DISTINCT(F_IdFact)) TOTAL";
        query += " FROM tb_factura WHERE F_StsFact='A' ";
        if(idUnidad!=null && !idUnidad.isEmpty()){
            query += " AND F_ClaCli = '"+idUnidad+"' ";
            //sqlParameterSource.addValue("idUnidad", idUnidad);
            
        }
        if(clave!=null && !clave.isEmpty()){
            query += " AND F_ClaPro = '"+clave+"' ";
            //sqlParameterSource.addValue("clave", clave);
        }
        if(fechaInicio!=null && !fechaInicio.isEmpty() && fechaFin!=null && !fechaFin.isEmpty()){
            query += " AND date(F_FecEnt) >= date('"+fechaInicio+"') ";
            query += " AND date(F_FecEnt) <= date('"+fechaFin+"') ";
        }
        
        System.out.println(query);
        return jdbcTemplate.queryForObject(query, Long.class);  
    }

    @Override
    public List<Map<String, Object>> getCompraByParameters(Integer idProveedor, String clave, String[] parametros, String fechaInicio, String fechaFin, Integer page, Integer offset) {
        String query = " SELECT ";
        if(parametros!=null && parametros.length>0){
            for(int i=0;i<parametros.length;i++){
                if(i==(parametros.length-1)){
                    query+=parametros[i];
                }else{
                    query+=parametros[i]+",";
                }
                
            }
        }

        query += " FROM tb_compra com WHERE com.F_StsCom='A' ";
        if(idProveedor!=null && idProveedor>0){
            query += " AND com.F_Provee= "+idProveedor;
            
        }
        if(clave!=null && !clave.isEmpty()){
            query += " AND com.F_ClaPro = '"+clave+"' ";
            //sqlParameterSource.addValue("clave", clave);
        }
        if(fechaInicio!=null && !fechaInicio.isEmpty() && fechaFin!=null && !fechaFin.isEmpty()){
            query += " AND date(com.F_FecApl) >= date('"+fechaInicio+"') ";
            query += " AND date(com.F_FecApl) <= date('"+fechaFin+"') ";
        }
        
        query += " LIMIT "+(page*offset)+","+offset;
        System.out.println(query);
        return jdbcTemplate.queryForList(query);  
    }

    @Override
    public Long getTotalCompraByParameters(Integer idProveedor, String clave, String fechaInicio, String fechaFin) {
        String query = " SELECT COUNT(DISTINCT(com.F_IdCom)) TOTAL ";

        query += " FROM tb_compra com WHERE com.F_StsCom='A' ";
        if(idProveedor!=null && idProveedor>0){
            query += " AND com.F_Provee= "+idProveedor;
            
        }
        if(clave!=null && !clave.isEmpty()){
            query += " AND com.F_ClaPro = '"+clave+"' ";
            //sqlParameterSource.addValue("clave", clave);
        }
        if(fechaInicio!=null && !fechaInicio.isEmpty() && fechaFin!=null && !fechaFin.isEmpty()){
            query += " AND date(com.F_FecApl) >= date('"+fechaInicio+"') ";
            query += " AND date(com.F_FecApl) <= date('"+fechaFin+"') ";
        }
        
        System.out.println(query);
        return jdbcTemplate.queryForObject(query,Long.class); 
    }
   
    @Override
    public List<Map<String, Object>> getSummatoryCompraByParameters(Integer idProveedor, String clave, String[] parametros, String fechaInicio, String fechaFin) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        String query = " SELECT ";
        
        if(parametros!=null && parametros.length>0){
            for(int i=0;i<parametros.length;i++){
                if(parametros[i]!=null){
                    switch(parametros[i]){
                        case "F_ImpTo":
                        case "F_Cajas":
                        //case "F_ComTot":
                        case "F_CanCom":
                        case "F_Costo":    
                            query+=" SUM(com."+parametros[i]+")"+parametros[i]+" ,";
                            break;    
                        case "F_ClaDoc":
                        case "F_ClaPro":
                        case "F_OrdCom":    
                            query+=" COUNT(DISTINCT(com."+parametros[i]+")) "+parametros[i]+" ,";
                            break;
                        default:
                            break;
                    }
                    
                }
            }
        }
        query += " COUNT(DISTINCT(com.F_IdCom)) TOTAL ";        
        //query += " FROM gnklmex_saa_lerma.tb_factura WHERE F_StsFact='A' ";

        
        query += " FROM tb_compra com WHERE com.F_StsCom='A' ";
        if(idProveedor!=null && idProveedor>0){
            query += " AND com.F_Provee= "+idProveedor;
            
        }
        if(clave!=null && !clave.isEmpty()){
            query += " AND com.F_ClaPro = '"+clave+"' ";
            //sqlParameterSource.addValue("clave", clave);
        }
        if(fechaInicio!=null && !fechaInicio.isEmpty() && fechaFin!=null && !fechaFin.isEmpty()){
            query += " AND date(com.F_FecApl) >= date('"+fechaInicio+"') ";
            query += " AND date(com.F_FecApl) <= date('"+fechaFin+"') ";
        }
        
        System.out.println(query);
        return jdbcTemplate.queryForList(query);  
 
    }

    @Override
    public List<Map<String, Object>> getReciboByParameters(Integer idProveedor, String clave, String[] parametros, String fechaInicio, String fechaFin) {
        String query = " SELECT ";
        if(parametros!=null && parametros.length>0){
            for(int i=0;i<parametros.length;i++){
                if(i==(parametros.length-1)){
                    query+=parametros[i];
                }else{
                    query+=parametros[i]+",";
                }
                
            }
        }

        query += " FROM tb_compra com WHERE com.F_StsCom='A' ";
        if(idProveedor!=null && idProveedor>0){
            query += " AND com.F_Provee= "+idProveedor;
            
        }
        if(clave!=null && !clave.isEmpty()){
            query += " AND com.F_ClaPro = '"+clave+"' ";
            //sqlParameterSource.addValue("clave", clave);
        }
        if(fechaInicio!=null && !fechaInicio.isEmpty() && fechaFin!=null && !fechaFin.isEmpty()){
            query += " AND date(com.F_FecApl) >= date('"+fechaInicio+"') ";
            query += " AND date(com.F_FecApl) <= date('"+fechaFin+"') ";
        }
        
        System.out.println(query);
        return jdbcTemplate.queryForList(query);  
    }
    
}
