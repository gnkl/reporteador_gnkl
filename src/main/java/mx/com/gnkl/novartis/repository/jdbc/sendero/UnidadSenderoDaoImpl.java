/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jdbc.sendero;

import java.util.List;
import java.util.Map;
import mx.com.gnkl.novartis.repository.UnidadDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 *
 * @author jmejia
 */
@Repository("unidadSenderoDao")
public class UnidadSenderoDaoImpl implements UnidadDao{

    @Autowired
    @Qualifier("jdbcTemplateSendero")
    private JdbcTemplate jdbcTemplate;
    
    @Override
    public List<Map<String, Object>> getAllUnidad() {
        String query = " SELECT F_ClaCli clave, F_NomCli nombre, F_Direc direc, F_Dir dir FROM tb_uniatn ";
        return jdbcTemplate.queryForList(query);        
    }
    
    /*@Override
    public List<Map<String, Object>> getAllBitacoraByUsuarioEnvio(Integer idUsuario,Integer idEnvio) {
        String query = " SELECT b.id, b.nombre,b.km_inicial,b.km_final,b.fecha,b.fecha_fin,b.id_ruta,b.id_status,b.is_final,b.is_pausa,b.activo, bs.des_status, r.nombre nombre_ruta, bs.num_status, b.is_final"+
        " FROM tb_bitacora b, tb_bitacora_status bs, tb_ruta r WHERE b.id_operador=:idOperador AND b.id_envio=:idEnvio AND b.id_status=bs.id AND bs.num_status NOT IN (8) AND b.id_ruta=r.id";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
        .addValue("idOperador", idUsuario)
        .addValue("idEnvio", idEnvio); 
        return namedTemplate.queryForList(query,sqlParameterSource);
    }     */

    @Override
    public List<Map<String, Object>> getAllUnidadByName(String name) {
        String query = " SELECT F_ClaCli clave, F_NomCli nombre, F_Direc direc FROM tb_uniatn WHERE F_NomCli LIKE ?";
        return jdbcTemplate.queryForList(query,new Object[]{"%"+name+"%"});        
    }

    @Override
    public List<Map<String, Object>> getAllClavesByUnidad(String idUnidad) {
        String query = " SELECT DISTINCT f.F_ClaPro AS clave,REPLACE(m.F_DesPro,'\n',' ') AS descripcion FROM tb_factura f, tb_medica m WHERE f.F_ClaCli=? AND f.F_CantSur>0 AND f.F_ClaPro=m.F_ClaPro ORDER BY f.F_ClaPro ";
        return jdbcTemplate.queryForList(query,new Object[]{idUnidad});
    }

    @Override
    public List<Map<String, Object>> getAllClaves() {
        String query = " SELECT DISTINCT f.F_ClaPro AS clave,REPLACE(m.F_DesPro,'\n',' ') AS descripcion FROM tb_factura f, tb_medica m WHERE f.F_CantSur>0 AND f.F_ClaPro=m.F_ClaPro ORDER BY f.F_ClaPro ";
        return jdbcTemplate.queryForList(query);
    }

    @Override
    public List<Map<String, Object>> getAllSummatoryUnidadByParameters(String tipo, Integer claveMunicipio, Integer dispensador) {
        String query = " SELECT COUNT(DISTINCT(med.F_ClaCli)) TOTAL ";
        
        //query += " FROM gnklmex_saa_lerma.tb_factura WHERE F_StsFact='A' ";
        query += " FROM tb_uniatn uni WHERE uni.F_StsCli='A' ";
        
        if(tipo!=null && !tipo.isEmpty()){
            query += " AND uni.F_Tipo = '"+tipo+"' ";
        }        

        if(claveMunicipio!=null && claveMunicipio>0){
            query += " AND uni.F_ClaMun = "+claveMunicipio;
        }        

        if(dispensador!=null && dispensador>0){
            query += " AND uni.F_idDispensador = "+dispensador;
        }        
        
        System.out.println(query);
        return jdbcTemplate.queryForList(query);   
    }

    @Override
    public List<Map<String, Object>> getAllUnidadByParameters(String tipo, Integer claveMunicipio, Integer dispensador, String[] parametros, Integer page, Integer offset) {
        String query = " SELECT ";
        MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
        if(parametros!=null && parametros.length>0){
            for(int i=0;i<parametros.length;i++){
                if(i==(parametros.length-1)){
                    query+=parametros[i];
                }else{
                    query+=parametros[i]+",";
                }
                
            }
        }

        query += " FROM tb_uniatn uni WHERE uni.F_StsCli='A' ";
        
        if(tipo!=null && !tipo.isEmpty()){
            query += " AND F_Tipo = '"+tipo+"' ";
        }
        
        if(claveMunicipio!=null && claveMunicipio>0){
            query += " AND F_ClaMun = "+claveMunicipio;
        }
        
        if(dispensador!=null && dispensador>0){
            query += " AND F_idDispensador = "+dispensador;
        }
        
        query += " LIMIT "+(page*offset)+","+offset;
        System.out.println(query);
        return jdbcTemplate.queryForList(query);  
    }

    @Override
    public Long getTotalUnidadByParameters(String tipo, Integer claveMunicipio, Integer dispensador) {
        String query = " SELECT COUNT(DISTINCT(F_ClaCli)) TOTAL";

        query += " FROM tb_uniatn uni WHERE uni.F_StsCli='A' ";
        
        if(tipo!=null && !tipo.isEmpty()){
            query += " AND F_Tipo = '"+tipo+"' ";
        }
        
        if(claveMunicipio!=null && claveMunicipio>0){
            query += " AND F_ClaMun = "+claveMunicipio;
        }
        
        if(dispensador!=null && dispensador>0){
            query += " AND F_idDispensador = "+dispensador;
        }

        System.out.println(query);
        return jdbcTemplate.queryForObject(query,Long.class); 
    }

    @Override
    public List<Map<String, Object>> getAllTipoUnidad() {
        String query = " SELECT DISTINCT(F_Tipo) as clave FROM tb_uniatn ";
        return jdbcTemplate.queryForList(query);
    }

    @Override
    public List<Map<String, Object>> getAllDispensadorList() {
        String query = " SELECT id clave, descripcion nombre FROM tb_dispensadores ";
        return jdbcTemplate.queryForList(query);
    }

    @Override
    public List<Map<String, Object>> getAllUnidadByParameters(String tipo, String[] parametros) {
        String query = " SELECT ";
        if(parametros!=null && parametros.length>0){
            for(int i=0;i<parametros.length;i++){
                if(i==(parametros.length-1)){
                    query+=parametros[i];
                }else{
                    query+=parametros[i]+",";
                }
                
            }
        }

        query += " FROM tb_uniatn uni WHERE uni.F_StsCli='A' ";
        
        if(tipo!=null && !tipo.isEmpty()){
            query += " AND F_Tipo = '"+tipo+"' ";
        }
        
        System.out.println(query);
        return jdbcTemplate.queryForList(query);  
        
    }
}
