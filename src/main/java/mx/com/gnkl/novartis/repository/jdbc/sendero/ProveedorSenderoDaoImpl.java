/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jdbc.sendero;

import java.util.List;
import java.util.Map;
import mx.com.gnkl.novartis.repository.ProveedorDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jmejia
 */
@Repository("proveedorSenderoDao")
public class ProveedorSenderoDaoImpl implements ProveedorDao{
    
    @Autowired
    @Qualifier("jdbcTemplateSendero")
    private JdbcTemplate jdbcTemplate;
    
    @Override
    public List<Map<String, Object>> getAllProveedor() {
        String query = " SELECT F_ClaProve clave, F_NomPro nombre FROM tb_proveedor tbp ";
        return jdbcTemplate.queryForList(query);
    }

    @Override
    public List<Map<String, Object>> getAllProveedorByName(String name) {
        String query = " SELECT F_ClaProve clave, F_NomPro nombre FROM tb_proveedor tbp WHERE  tbp.F_NomPro LIKE ?";
        return jdbcTemplate.queryForList(query,new Object[]{"%"+name+"%"});
    }

    @Override
    public List<Map<String, Object>> getAllClavesByProveedor(Integer idProveedor) {
        String query = " SELECT DISTINCT medica.F_ClaPro AS clave,REPLACE(medica.F_DesPro,'\\n',' ') AS descripcion FROM tb_prodprov prodprov, tb_medica medica WHERE prodprov.F_ClaProve=? AND medica.F_ClaPro=prodprov.F_ClaPro ";
        return jdbcTemplate.queryForList(query,new Object[]{idProveedor});
    }

    @Override
    public List<Map<String, Object>> getAllClaves() {
        String query = " SELECT DISTINCT f.F_ClaPro AS clave,REPLACE(m.F_DesPro,'\n',' ') AS descripcion FROM tb_factura f, tb_medica m WHERE f.F_CantSur>0 AND f.F_ClaPro=m.F_ClaPro ORDER BY f.F_ClaPro ";
        return jdbcTemplate.queryForList(query);
    }
    
}
